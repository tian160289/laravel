var Datepicker = function(){

    return {
        //Datepickers
        initDatepicker: function(){
	        $('#date').datepicker({
				dateFormat: 'mm/dd/yy',
				prevText: '<i class="fa fa-angle-left"></i>',
	            nextText: '<i class="fa fa-angle-right"></i>',
	            changeMonth: true,
				changeYear: true,
				yearRange: "-65:-17",
				dayNamesMin: [ "Min", "Sen", "Sel", "Rab", "Kam", "Jum", "Sab" ],
				monthNamesShort: [ "Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember" ]
	        });
			$('#date').focus(function(){
				$('#date').blur();
			});
        }

    };
}();

var InquiriresForm = function () {

    return {
        
        //Inquirires Form
        initInquiriresForm: function () {
	        // Validation Indonesian Language
	        $("#inquiriesform").validate({
	            // Rules for form validation
	            rules:
	            {
	                persontitle:
	                {
	                    required: true
	                },
	                fullname:
	                {
	                    required: true
	                },
					datebirth:
	                {
	                    required: true
	                },
					email:
	                {
	                    required: true,
	                    email: true
	                },
	                mobilephone:
	                {
	                    required: true,
						number: true
	                },
					personaddress:
	                {
	                    required: true
	                },
					region:
	                {
	                    required: true
	                },
					companyname:
	                {
	                    required: true
	                },
	                companyindustry:
	                {
	                    required: true
	                },
	                interesttype:
	                {
	                    required: true
	                },
					interestdesc:
	                {
	                    required: true
	                },
					txtCaptcha:
	                {
	                    required: true
	                }
	            },

	            // Messages for form validation
	            messages:
	            {
	                fullname:
	                {
	                    required: 'Mohon mengisi nama lengkap Anda'
	                },
					datebirth:
	                {
	                    required: 'Mohon mengisi nama tanggal lahir Anda'
	                },
	                email:
	                {
	                    required: 'Mohon mengisi alamat email Anda',
	                    email: 'Alamat email yang Anda masukan salah'
	                },
	                mobilephone:
	                {
	                    required: 'Mohon mengisi nomor telepon genggam Anda',
						number: 'Nomor telepon genggam harus dimasukan dengan angka'
	                },
	                personaddress:
	                {
	                    required: 'Mohon mengisi alamat lengkap rumah/kantor Anda'
	                },
					region:
	                {
	                    required: 'Mohon mengisi wilayah dari rumah/kantor Anda'
	                },
					companyname:
	                {
	                    required: 'Mohon mengisi nama perusahaan Anda'
	                },
	                companyindustry:
	                {
	                    required: 'Mohon mengisi jenis industri perusahaan Anda'
	                },
	                interesttype:
	                {
	                    required: 'Mohon mengisi ketertarikan Anda'
	                },
					interestdesc:
	                {
	                    required: 'Mohon menjelaskan pertanyaan Anda'
	                },
					txtCaptcha:
	                {
	                    required: 'Mohon mencantumkan kode yang tertera'
	                }
	            },

	            // Ajax form submition
	            /*submitHandler: function(form)
	            {
	                $(form).ajaxSubmit(
	                {
	                    beforeSend: function()
	                    {
	                        $('#inquiriesform button[type="submit"]').addClass('button-uploading').attr('disabled', true);
	                    },
	                uploadProgress: function(event, position, total, percentComplete)
	                {
	                    $("#inquiriesform .progress").text(percentComplete + '%');
	                },
	                    success: function()
	                    {
	                        $("#inquiriesform").addClass('submited');
	                        $('#inquiriesform button[type="submit"]').removeClass('button-uploading').attr('disabled', false);
	                    }
	                });
	            },  */
	            
	            // Do not change code below
				errorPlacement: function(error, element){
					if(element.attr("type") == "radio" || element.attr("type") == "checkbox") {
						error.appendTo(element.parents("div.placeafter"));
					}
					else {
						error.insertAfter(element.parent());
					}
				}
	        });
			
			// Validation English Language
			$("#inquiriesform-en").validate({
	            // Rules for form validation
	            rules:
	            {
	                persontitle:
	                {
	                    required: true
	                },
	                fullname:
	                {
	                    required: true
	                },
					datebirth:
	                {
	                    required: true
	                },
					email:
	                {
	                    required: true,
	                    email: true
	                },
	                mobilephone:
	                {
	                    required: true,
						number: true
	                },
					personaddress:
	                {
	                    required: true
	                },
					region:
	                {
	                    required: true
	                },
					companyname:
	                {
	                    required: true
	                },
	                companyindustry:
	                {
	                    required: true
	                },
	                interesttype:
	                {
	                    required: true
	                },
					interestdesc:
	                {
	                    required: true
	                },
					txtCaptcha:
	                {
	                    required: true
	                }
	            },

	            // Ajax form submition
	            /*submitHandler: function(form)
	            {
	                $(form).ajaxSubmit(
	                {
	                    beforeSend: function()
	                    {
	                        $('#inquiriesform button[type="submit"]').addClass('button-uploading').attr('disabled', true);
	                    },
	                uploadProgress: function(event, position, total, percentComplete)
	                {
	                    $("#inquiriesform .progress").text(percentComplete + '%');
	                },
	                    success: function()
	                    {
	                        $("#inquiriesform").addClass('submited');
	                        $('#inquiriesform button[type="submit"]').removeClass('button-uploading').attr('disabled', false);
	                    }
	                });
	            },  */
	            
	            // Do not change code below
				errorPlacement: function(error, element){
					if(element.attr("type") == "radio" || element.attr("type") == "checkbox") {
						error.appendTo(element.parents("div.placeafter"));
					}
					else {
						error.insertAfter(element.parent());
					}
				}
	        });
        }

    };

}();
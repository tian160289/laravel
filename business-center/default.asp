<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->  
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->  
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->  
<head>
    <title>Biznet Technovillage | Biznet Techspace Business Center</title>

    <!--#INCLUDE FILE="../include/meta-tag-en.asp"-->

    <!--#INCLUDE FILE="../include/global-style.asp"-->

	<!--#INCLUDE FILE="../include/google-analytics.asp"-->

	<!--#INCLUDE FILE="../include/connect.asp"-->

	<!-- CSS Implementing Page -->
	<link rel="stylesheet" href="/assets/plugins/revolution-slider/rs-plugin/css/settings.css" type="text/css" media="screen">
    <!--[if lt IE 9]><link rel="stylesheet" href="/assets/plugins/revolution-slider/rs-plugin/css/settings-ie8.css" type="text/css" media="screen"><![endif]-->
</head>

<body>
<div class="wrapper">
    <!--#INCLUDE FILE="../inc-header.asp"-->

       <!--=== Breadcrumbs ===-->
    <div class="breadcrumbs">
        <div class="container">
            <h1 class="pull-left">Business Center</h1>
            <ul class="pull-right breadcrumb">
                <li><a href="/">Home</a></li>
                <li class="active">Business Center</li>
            </ul>
        </div>
    </div><!--/breadcrumbs-->
    <!--=== End Breadcrumbs ===-->

	<!--=== Slider ===-->
    <div class="tp-banner-container">
        <div class="tp-banner">
            <ul>
				<!-- SLIDE -->
                <li class="revolution-mch-1" data-transition="fade" data-slotamount="5" data-masterspeed="1000" data-title="Slide 1">
                    <!-- MAIN IMAGE -->
                    <img src="/assets/img/main/business-center/Techspace.jpg" alt="Biznet Techspace" data-bgfit="cover" data-bgposition="center top" data-bgrepeat="no-repeat">

                    <div class="tp-caption revolution-ch1 sft start"
                        data-x="center"
                        data-hoffset="0"
                        data-y="100"
                        data-speed="1500"
                        data-start="500"
                        data-easing="Back.easeInOut"
                        data-endeasing="Power1.easeIn"                        
                        data-endspeed="300">
						Biznet Techspace Cibubur
                    </div>

                    <!-- LAYER -->
                    <div class="tp-caption revolution-ch2 sft"
                        data-x="center"
                        data-hoffset="0"
                        data-y="190"
                        data-speed="1400"
                        data-start="2000"
                        data-easing="Power4.easeOut"
                        data-endspeed="300"
                        data-endeasing="Power1.easeIn"
                        data-captionhidden="off"
                        style="z-index: 6">
						Full Service Business Center
                    </div>

                </li>
                <!-- END SLIDE -->

				<!-- SLIDE -->
                <li class="revolution-mch-1" data-transition="fade" data-slotamount="5" data-masterspeed="1000" data-title="Disaster Recovery Center">
                    <!-- MAIN IMAGE -->
                    <img src="/assets/img/space/main_techspace_disasterrecovery.jpg"  alt="Disaster Recovery Center"  data-bgfit="cover" data-bgposition="center top" data-bgrepeat="no-repeat">

                    <div class="tp-caption revolution-ch1 sft start"
                        data-x="center"
                        data-hoffset="0"
                        data-y="100"
                        data-speed="1500"
                        data-start="500"
                        data-easing="Back.easeInOut"
                        data-endeasing="Power1.easeIn"                        
                        data-endspeed="300">
                        Disaster Recovery Center
                    </div>

                    <!-- LAYER -->
                    <div class="tp-caption revolution-ch2 sft"
                        data-x="center"
                        data-hoffset="0"
                        data-y="190"
                        data-speed="1400"
                        data-start="2000"
                        data-easing="Power4.easeOut"
                        data-endspeed="300"
                        data-endeasing="Power1.easeIn"
                        data-captionhidden="off"
                        style="z-index: 6">
                        Shared and Dedicated Working Space
                    </div>

                </li>
                <!-- END SLIDE --> 

            </ul>
            <div class="tp-bannertimer tp-bottom"></div>            
        </div>
    </div>
    <!--=== End Slider ===-->

    <!--=== Content Part ===-->
    <div class="container content">
		<div class="headline-center-v2 margin-bottom-60">
            <h2>About <span class="color-green">Biznet Techspace</span></h2>
            <span class="bordered-icon"><i class="fa fa-th-large"></i></span>
            <p>Biznet Techspace is a modern business center owned and operated by <a href="http://www.biznetdatacenter.com">Biznet Data Center</a>, targeted for startup technologies companies that need physical office location for the operation as well as the disaster recovery center for enterprises and small medium businesses.</p>
			<p>Biznet Techspace offers shared or dedicated office to run the business, equipped with secretarial service, print & copy, desktop computer and wireless Internet to ensure the business run smoothly. Facilities can be used by member or guest (paid by daily usage).</p>
        </div>
    </div>

    <!-- Flat Background Block -->
    <div class="bg-grey margin-bottom-20">
        <div class="container content-sm">
			<div class="headline-center-v2">
				<h2>Disaster Recovery</h2>
				<span class="bordered-icon"><i class="fa fa-th-large"></i></span>
			</div>
            <div class="row margin-bottom-20">
                <div class="col-md-12">
					<p>Biznet Techspace offers shared and dedicated working space, communication and managed services to ensure the business keep running when there is disaster or emergency.</p>
					<p>Managed services is a management strategy by which an organization outsources major, non-core functions to specialized and efficient service provider.</p>
					<p>Companies will seek to form partnership with Managed Services providers whose capabilities exceed their own. Companies will contract for shared access to resources that are beyond their individual reach, whether it is building/facility, technology, or people. Biznet offers telecommunication, Data Center and managed services to support Disaster Recovery requirements.</p>
					<p>For some companies with high data security needs i.e ISO 27001:2005, PCI-DSS, or HIPAA, they will need most reliable and trustworthy business partner with IT infrastructure and data recovery system to manage the unexpected.</p>
                </div>
            </div>
			<div class="row">
				<div class="col-sm-6">
					<ul class="list-unstyled checked-list">
						<li><i class="fa fa-check color-green"></i> Access to world class facility, technology, and/or skilled IT engineers</li>
						<li><i class="fa fa-check color-green"></i> Partner that will allow you focus on improving your core business</li>
						<li><i class="fa fa-check color-green"></i> Ways to reduce and control IT operating costs</li>
					</ul>
				</div>
				<div class="col-sm-6">
					<ul class="list-unstyled checked-list">
						<li><i class="fa fa-check color-green"></i> Access to skilled IT engineers and/or technologies not available internally</li>
						<li><i class="fa fa-check color-green"></i> Ways to accelerate reengineering effort, and/or</li>
						<li><i class="fa fa-check color-green"></i> Ways to reduce and control IT capital costs</li>
					</ul>
				</div>    
			</div>
        </div><!--/end container-->
    </div>
    <!-- End Flat Background Block -->

    <div class="content-sm">
        <div class="container">
            <div class="row margin-bottom-60">
                <div class="col-md-6">
                    <div class="responsive-video margin-bottom-30">
                        <img src="/assets/img/main/business-center/Meetingroom.jpg" class="img-responsive" alt=""/>
					</div>
                </div>
                <div class="col-md-6">
                    <h2 class="headline-brd">Meeting Rooms</h2>
                    <p>Biznet Techspace offers several meeting rooms that can fit 6 - 8 persons per room.</p>
                    	<ul class="list-unstyled checked-list">
                            <li><i class="fa fa-check color-green"></i> Select your layout options like U-shape, theater, classroom and boardroom style</li>
                            <li><i class="fa fa-check color-green"></i> Professional business environment</li>
                            <li><i class="fa fa-check color-green"></i> Spacious lounge and lobby area, supported by friendy Customer Service Officer</li>
                        </ul>
                    <p>All our meeting rooms equipped with Wireless Broadband Internet, LCD Screen, flipchart, whiteboard and stationery</p>
                </div>
            </div>

			<div class="tag-box tag-box-v2">
				<p>For more information about Biznet Techspace and services, please contact our Account Executive via phone +62-21-57998888, email <a class="link" href="mailto:techspace@biznetnetworks.com">techspace@biznetnetworks.com</a> or visit our nearest <a href="http://www.biznetnetworks.com/en/company/contact">branch office</a> in your city.</p>
				<p>For Biznet Technovillage facility tour, please fill out the <a href="http://www.biznetnetworks.com/en/company/technovillage-tour">registration form</a>.</p>
			</div>
        </div><!--/end container-->    
	</div>

    <!--#INCLUDE FILE="../inc-footer.asp"-->
</div><!--/wrapper-->

<!--#INCLUDE FILE="../include/global-script.asp"-->

<!-- JS Implementing Plugins -->
<script type="text/javascript" src="/assets/plugins/revolution-slider/rs-plugin/js/jquery.themepunch.tools.min.js"></script>
<script type="text/javascript" src="/assets/plugins/revolution-slider/rs-plugin/js/jquery.themepunch.revolution.min.js"></script>
<!-- JS Page Level -->
<script type="text/javascript" src="/assets/js/plugins/revolution-slider.js"></script>
<script type="text/javascript">
    jQuery(document).ready(function() {
        RevolutionSlider.initRSfullWidth();
    });
</script>
<!--[if lt IE 9]>
    <script src="/assets/plugins/respond.js"></script>
    <script src="/assets/plugins/html5shiv.js"></script>
    <script src="/assets/js/plugins/placeholder-IE-fixes.js"></script>
<![endif]-->

</body>
</html>
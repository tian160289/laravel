<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->  
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->  
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->  
<head>
    <title>Biznet Technovillage | Concept</title>

    <!--#INCLUDE FILE="../include/meta-tag-en.asp"-->

    <!--#INCLUDE FILE="../include/global-style.asp"-->

	<!--#INCLUDE FILE="../include/google-analytics.asp"-->

	<!--#INCLUDE FILE="../include/connect.asp"-->

	<!-- CSS Implementing Page -->
	<link rel="stylesheet" href="/assets/plugins/revolution-slider/rs-plugin/css/settings.css" type="text/css" media="screen">
    <!--[if lt IE 9]><link rel="stylesheet" href="/assets/plugins/revolution-slider/rs-plugin/css/settings-ie8.css" type="text/css" media="screen"><![endif]-->
</head>

<body>
<div class="wrapper">
	<!--#INCLUDE FILE="../inc-header.asp"-->

	<!--=== Breadcrumbs ===-->
	<div class="breadcrumbs">
		<div class="container">
			<h1 class="pull-left">Concept</h1>
			<ul class="pull-right breadcrumb">
				<li><a href="/">Home</a></li>
				<li class="active">Concept</li>
			</ul>
		</div>
	</div><!--/breadcrumbs-->
	<!--=== End Breadcrumbs ===-->

	<!--=== Slider ===-->
    <div class="tp-banner-container">
        <div class="tp-banner">
            <ul>
				<!-- SLIDE -->
                <li class="revolution-mch-1" data-transition="fade" data-slotamount="5" data-masterspeed="1000">
                    <!-- MAIN IMAGE -->
                    <img src="/assets/img/main/banner-biznet-datacenter-technovillage-front-view.jpg"  alt="darkblurbg"  data-bgfit="cover" data-bgposition="center top" data-bgrepeat="no-repeat">

                    <div class="tp-caption revolution-ch1 sft start"
                        data-x="center"
                        data-hoffset="0"
                        data-y="100"
                        data-speed="1500"
                        data-start="500"
                        data-easing="Back.easeInOut"
                        data-endeasing="Power1.easeIn"                        
                        data-endspeed="300">
                        Biznet Data Center Technovillage
                    </div>

                    <!-- LAYER -->
                    <div class="tp-caption revolution-ch2 sft"
                        data-x="center"
                        data-hoffset="0"
                        data-y="190"
                        data-speed="1400"
                        data-start="2000"
                        data-easing="Power4.easeOut"
                        data-endspeed="300"
                        data-endeasing="Power1.easeIn"
                        data-captionhidden="off"
                        style="z-index: 6">
						Indonesia's First Tier-3 Green Data Center
                    </div>
                </li>
                <!-- END SLIDE -->

				<!-- SLIDE -->
                <li class="revolution-mch-1" data-transition="fade" data-slotamount="5" data-masterspeed="1000">
                    <!-- MAIN IMAGE -->
                    <img src="/assets/img/main/Concept/Landscape_2.jpg"  alt="Biznet Technovillage Green Landscape" data-bgfit="cover" data-bgposition="left top" data-bgrepeat="no-repeat">

                    <div class="tp-caption revolution-ch1 sft start"
                        data-x="center"
                        data-hoffset="0"
                        data-y="100"
                        data-speed="1500"
                        data-start="500"
                        data-easing="Back.easeInOut"
                        data-endeasing="Power1.easeIn"                        
                        data-endspeed="300">
						Landscape
                    </div>

                    <!-- LAYER -->
                    <div class="tp-caption revolution-ch2 sft"
                        data-x="center"
                        data-hoffset="0"
                        data-y="190"
                        data-speed="1400"
                        data-start="2000"
                        data-easing="Power4.easeOut"
                        data-endspeed="300"
                        data-endeasing="Power1.easeIn"
                        data-captionhidden="off"
                        style="z-index: 6">
						Enhancing Creative Working Environment
                    </div>
                </li>
                <!-- END SLIDE -->

				<!-- SLIDE -->
                <li class="revolution-mch-1" data-transition="fade" data-slotamount="5" data-masterspeed="1000">
                    <!-- MAIN IMAGE -->
                    <img src="/assets/img/main/Concept/Landscape_1.jpg"  alt="Green Environment" data-bgfit="cover" data-bgposition="left top" data-bgrepeat="no-repeat">

                    <div class="tp-caption revolution-ch1 sft start"
                        data-x="center"
                        data-hoffset="0"
                        data-y="100"
                        data-speed="1500"
                        data-start="500"
                        data-easing="Back.easeInOut"
                        data-endeasing="Power1.easeIn"                        
                        data-endspeed="300">
						Green Environment
                    </div>

                    <!-- LAYER -->
                    <div class="tp-caption revolution-ch2 sft"
                        data-x="center"
                        data-hoffset="0"
                        data-y="190"
                        data-speed="1400"
                        data-start="2000"
                        data-easing="Power4.easeOut"
                        data-endspeed="300"
                        data-endeasing="Power1.easeIn"
                        data-captionhidden="off"
                        style="z-index: 6">
						Maximum energy efficiency and<br />minimum environmental impact
                    </div>
                </li>
                <!-- END SLIDE -->
				
				<!-- SLIDE -->
                <li class="revolution-mch-1" data-transition="fade" data-slotamount="5" data-masterspeed="1000">
                    <!-- MAIN IMAGE -->
                    <img src="/assets/img/technovillage/main_biznet_technovillage_voc.jpg"  alt="darkblurbg"  data-bgfit="cover" data-bgposition="left top" data-bgrepeat="no-repeat">

                    <div class="tp-caption revolution-ch1 sft start"
                        data-x="center"
                        data-hoffset="0"
                        data-y="100"
                        data-speed="1500"
                        data-start="500"
                        data-easing="Back.easeInOut"
                        data-endeasing="Power1.easeIn"                        
                        data-endspeed="300">
                        Video Operation Center
                    </div>

                    <!-- LAYER -->
                    <div class="tp-caption revolution-ch2 sft"
                        data-x="center"
                        data-hoffset="0"
                        data-y="190"
                        data-speed="1400"
                        data-start="2000"
                        data-easing="Power4.easeOut"
                        data-endspeed="300"
                        data-endeasing="Power1.easeIn"
                        data-captionhidden="off"
                        style="z-index: 6">
						Monitor Video Downlink & <br/>
						Uplink for Biznet Cable TV Platform
                    </div>
                </li>
                <!-- END SLIDE -->

            </ul>
            <div class="tp-bannertimer tp-bottom"></div>            
        </div>
    </div>
    <!--=== End Slider ===-->

  	<!--=== Content Part ===-->
    <div class="container content"> 
		<div class="headline-center-v2">
            <h2>Biznet Technovillage <span class="color-green">Concept</span></h2>
            <span class="bordered-icon"><i class="fa fa-th-large"></i></span>
            <p>Biznet Technovillage is an integrated high tech facility located 35 KM South of Jakarta, consists of Tier 3 Green Data Center, Grade A Office Space and Full Service Business Center targeted for research center, backup facility, media production and outsourcing center.</p>
			<p>Biznet Technovillage is using several green building technologies to reduce waste and power such as furniture material from existing pallet wood, LED based light bulbs to reduce power consumption, Dynamic Rotary Uninterruptible Power Supply to provide 100% backup power without using any lead (Pb) based battery. Lead, at certain exposure levels, is a poisonous substance to animals as well as for human beings. The facility also reviewed by Green Building Council Indonesia for certification.</p>
        </div>
    </div> 

	<!--=== Parallax Backgound ===-->
	<div class="bg-image-v2 parallaxBg1">
        <div class="container">
            <div class="headline-center-v2 headline-center-v2-dark margin-bottom-20">
                <h2>WHAT IS TECHNOPARK?</h2>
                <span class="bordered-icon"><i class="fa fa-th-large"></i></span>
                <p>A technopark is a technology industrial complex that have the following characteristics.</p>

		        <div class="row">
		            <div class="col-md-6 content-boxes-v6 md-margin-bottom-10">
		                <i class="rounded-x icon-link"></i>
		                <h1 class="title-v3-md text-uppercase margin-bottom-10">Technology Industy</h1>
		                <p>The company is related with ICT, Media and Biotech industry.</p>
		            </div>
		            <div class="col-md-6 content-boxes-v6 md-margin-bottom-10">
		                <i class="rounded-x icon-paper-plane"></i>
		                <h2 class="title-v3-md text-uppercase margin-bottom-10">Excellent Location</h2>
		                <p>Strategically located so can be accessed from anywhere.</p>
		            </div>
		            <div class="col-md-6 content-boxes-v6">
		                <i class="rounded-x icon-refresh"></i>
		                <h2 class="title-v3-md text-uppercase margin-bottom-10">Excellent Infrastructure</h2>
		                <p>Excellent infrastructure and facility.</p>
		            </div>
		            <div class="col-md-6 content-boxes-v6">
		                <i class="rounded-x icon-refresh"></i>
		                <h2 class="title-v3-md text-uppercase margin-bottom-10">Creative Environment</h2>
		                <p>Working culture and environment that will enhance people to become more creative.</p>
		            </div>
		    </div><!--/container-->
                
            </div><!--/Headline Center V2-->   
        </div><!--/container-->
    </div>
    <!--=== End Parallax Backgound ===-->

	<div class="container content">
		<div class="headline-center-v2">
            <h2>BIZNET TECHNOVILLAGE <span class="color-green">FEATURES</span></h2>
            <span class="bordered-icon"><i class="fa fa-th-large"></i></span>
        </div>

        <!-- Service Blocks -->
        <div class="row content-boxes-v2 margin-bottom-30">
            <div class="col-md-3 md-margin-bottom-30">                
                <h2 class="heading-sm">
                    <a class="link-bg-icon" href="#">
                        <i class="icon-custom icon-sm rounded-x icon-bg-dark fa fa-star-half-o"></i>
                        <span>Perfect location in Jakarta's suburban area</span>
                    </a>
                </h2>
                <p class="text-justify"> Located in Cimanggis, West Java, about 35 KM South from Jakarta and can be reached within 45 minutes</p>
            </div>
             
            <div class="col-md-3 md-margin-bottom-30">
                <h2 class="heading-sm">
                    <a class="link-bg-icon" href="#">
                        <i class="icon-custom icon-sm rounded-x icon-bg-dark fa fa-car"></i> 
                        <span>Major Highway Access</span>
                    </a>
                </h2>
                <p class="text-justify">The development is located on Jakarta Bogor Ciawi (Jagorawi) Highway and Jakarta Outer Ring Road 2 (JORR2) Highways interchange nearby</p>
            </div>
             
             <div class="col-md-3 md-margin-bottom-30">
                <h2 class="heading-sm">
                    <a class="link-bg-icon" href="#">
                        <i class="icon-custom icon-sm rounded-x icon-bg-dark fa fa-arrows"></i> 
                        <span>Flexible Master Plan</span>
                    </a>
                </h2>
                <p class="text-justify">A development master plan around 20,000 m2 of land area, expandable to much larger space area</p>
            </div>
             
            <div class="col-md-3">
                <h2 class="heading-sm">
                    <a class="link-bg-icon" href="#">
                        <i class="icon-custom icon-sm rounded-x icon-bg-dark fa fa-building"></i>
                        <span>Building with Campus Block Concept</span>
                    </a>
                </h2>
                <p class="text-justify">a 3 - 5 stories building with approximate size of 1,000 m2 per floor space</p>
            </div>
        </div>
 
        <div class="row content-boxes-v2 margin-bottom-30">
            <div class="col-md-3 md-margin-bottom-30">                
                <h2 class="heading-sm">
                    <a class="link-bg-icon" href="#">
                        <i class="icon-custom icon-sm rounded-x icon-bg-dark fa fa-graduation-cap"></i>
                        <span>Leading IT, Multimedia and Telecom Education Centers</span>
                    </a>
                </h2>
                <p class="text-justify">Several leading universities and training providers within the complex to support the skilled workforce for the area and around 20 minutes from University of Indonesia, number one university in Indonesia</p>
            </div>
           
            <div class="col-md-3 md-margin-bottom-30">
                <h2 class="heading-sm">
                    <a class="link-bg-icon" href="#">
                        <i class="icon-custom icon-sm rounded-x icon-bg-dark fa fa-tree"></i> 
                        <span>Large Green Area Landscape</span>
                    </a>
                </h2>
                <p class="text-justify">30% from total landscape for natural park, water garden and open space</p>
            </div>
             
            <div class="col-md-3 md-margin-bottom-30">
                <h2 class="heading-sm">
                    <a class="link-bg-icon" href="#">
                        <i class="icon-custom icon-sm rounded-x icon-bg-dark fa fa-coffee"></i> 
                        <span>Onsite Amenities and Services</span>
                    </a>
                </h2>
                <p class="text-justify">General facilities such as Business Center, Dining Lounge and Coffee Shop</p>
            </div>
             
            <div class="col-md-3">
                <h2 class="heading-sm">
                    <a class="link-bg-icon" href="#">
                        <i class="icon-custom icon-sm rounded-x icon-bg-dark fa fa-soccer-ball-o"></i>
                        <span>Sport Facilities & Recreation Area for Better Lifestyle</span>
                    </a>
                </h2>
                <p class="text-justify">The facility will be provided to all tenants to enhance employee's health and lifestyle</p>
            </div>
        </div>
        <!-- End Service Blokcs --> 

        <hr>
		
		<div class="headline-center-v2">
            <h2><span class="color-green">GREEN</span> INITIATIVE</h2>
            <span class="bordered-icon"><i class="fa fa-th-large"></i></span>
        </div>
		
		<div class="row">
			<div class="col-md-6">
				<h2>Why do we use LED Lighting? Save Energy. Longer Light Lifetime.</h2>                        
				<p>Biznet Technovillage uses LED lighting to save energy consumption, throughout the building including outdoor streetlight. LED is more environmentally friendly than the regular light bulbs as it turns 15-25% of electricity consumption into a light, twice as many as regular light bulbs with only 8%.</p>
				<p>LED light bulbs can also last until 50,000 hours, or 30% longer than the usual light bulbs, and even 10 times longer than energy saving light bulbs. More over, LED light bulbs can last for 25 years. LED has more efficiency compare to tungsten lamp or fluorescent lamp. 1 watt of LED light can produce 100 lumens, by only consuming 80% of energy than the usual lamp. 1 LED lamp with 3,5 watt of power consumption can produce 50 watt lights of Halogen lamp.</p>
			</div>
			<div class="col-md-6">
				<img alt="" class="img-responsive margin-bottom-10" src="/assets/img/main/2.jpg">
			</div>
		</div>
		
		<hr class="devider devider-db">
		
		<div class="row">
			<div class="col-md-6">
				<img alt="" class="margin-bottom-10 img-responsive" src="/assets/img/main/5.jpg">
			</div>
			<div class="col-md-6">
				<h2>Why do we save Trees during Construction? Save Trees. Save the World. Save Yourself.</h2>
				<p>Biznet Technovillage realizes that trees are priceless for the existence of any living creature on earth. We preserve trees the best way that we can, making sure that nature and human beings are in equal balance. Oxygen production, cleaning the soil, controlling noise pollution, slowing down storm water runoff, acting as carbon sinks, cleaning the air, providing shade and coolness, being windbreaks and fighting soil erosion can be considered as the first few reasons why trees are priceless. At Biznet Technovillage, no trees are wasted, all trees are well treated, re-planted and maintained, to be the source of inspiration for human beings in creating technology and science.</p>
			</div>
		</div>
		
		<hr class="devider devider-db">
		
		<div class="row margin-bottom-60">
			<div class="col-md-6">
				<h2>Why do we invest in Green Data Center technologies? Maximum Energy Efficient. Minimum Environmental Impact</h2>
				<p>As the first Tier-3 Green Data Center in Indonesia, we would like to run our facility differently than others by using newer and greener technologies. Biznet Technovillage is designed to become a green data center, which is repository for the storage, management, and dissemination of data in which the mechanical, lighting, electrical, computer systems, and most of other aspects are designed for minimum usage of energy and to be environmentally friendly.</p>
				<p>All of us in Biznet Technovillage are always creating new innovations to ensure that all facilities are in line with green technology requirements. We used Dynamic Rotary Uninterruptable Power System (DRUPS), LED lighting, no carpet flooring, furniture from recycle wood pallets, preserve lots of trees during construction, and minimize water usage.</p> 
			</div>
			<div class="col-md-6">
				<img alt="" class="img-responsive" src="/assets/img/main/1.jpg">
			</div>
		</div>
		
		<hr class="devider devider-db">
		
		<div class="row">
			<div class="col-md-6">
				<img alt="" class="img-responsive" src="/assets/img/main/6.jpg">
			</div>
			<div class="col-md-6">
				<h2>How do we use efficient water? Save Water. Secure the Future.</h2>
				<p>Human needs water to grow plants, provide power, control fire uses water, and the most important is to stay alive. Since the early 20th century, Earth's mean surface temperature has increased by about 0.8 oC (1.4 oF), with about two-thirds of the increase occurring since 1980. It gives some impact for human life, but the most important impact is sustainability of water supplies. As our population growth, more and more people are using up this limited resource, and it was predicted that later, over 2.1 billion people have no access to clean drinking water and one child dies every 8 seconds from drinking dirty water.</p>
				<p>This is the right reason Biznet Technovillage takes a giant step to use water wisely. We believe that if we can't control the amount of water we truly need, we can control the amount of water that we're wasting. Nothing can replace water. There are many reasons why we have to save water. Even the earth is covered with water, but this water is in the dorm of frozen icebergs and glaciers, just 1% of all the water on Earth that can be used by people, the rest is salt water and people can't use it. So, what are you waiting for? Save water, secure the future!</p> 
			</div>
		</div>
		
		<hr class="devider devider-db">
		
		<div class="row">
			<div class="col-md-6">
				<h2>Why we don't use Carpet for Flooring? Use Solid Flooring. Healthier Air Quality. </h2>
				<p>At Biznet Technovillage, we don't use carpets for flooring. We use solid flooring such as wood, stone and vinyl to provide cleaner working environment. Wood flooring has many advantages over types of flooring. Wood adds warmth, simple and modern design that last more than 100 years with regular maintenance and repair if needed.</p>
				<p>Carpet flooring contains chemical ingredients called Formaldehid that can easily effects human if it combines with protein and causing eye irritation, sorethroat, asthma, allergic and such. It also quite difficult to clean and dust stays within the carpet, which caused dirty and smell.</p>
			</div>
			<div class="col-md-6">
				<img alt="" class="img-responsive margin-bottom-10" src="/assets/img/main/3.jpg">
			</div>
		</div>
		
		<hr class="devider devider-db">
		
		<div class="row">
			<div class="col-md-6">
				<img alt="" class="img-responsive margin-bottom-10" src="/assets/img/main/4.jpg">
			</div>
			<div class="col-md-6">
				<h2>How do we recycle used materials? Recycle to Save The Earth</h2>
			   <p>Biznet Technovillage imported lots of equipment from abroad, which were shipped using wood pallets. We use those wood pallets to make our interior such as wall covering, furniture, sofa and cabinet used in the coffee shop and office.</p>
            </div>
        </div>

	</div>

	<!--#INCLUDE FILE="../inc-footer.asp"-->
</div><!--/wrapper-->

<!--#INCLUDE FILE="../include/global-script.asp"-->

<!-- JS Implementing page -->
<script type="text/javascript" src="/assets/plugins/jquery.parallax.js"></script>
<script type="text/javascript" src="/assets/plugins/revolution-slider/rs-plugin/js/jquery.themepunch.tools.min.js"></script>
<script type="text/javascript" src="/assets/plugins/revolution-slider/rs-plugin/js/jquery.themepunch.revolution.min.js"></script>

<!-- JS Page Level -->           
<script type="text/javascript" src="/assets/js/plugins/revolution-slider.js"></script>
<script type="text/javascript">
    jQuery(document).ready(function() {
        App.initParallaxBg();        
        RevolutionSlider.initRSfullWidth();
    });
</script>
<!--[if lt IE 9]>
    <script src="/assets/plugins/respond.js"></script>
    <script src="/assets/plugins/html5shiv.js"></script>
    <script src="/assets/js/plugins/placeholder-IE-fixes.js"></script>
<![endif]-->

</body>
</html>
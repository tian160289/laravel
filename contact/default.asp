<%
	session("inquiries")="1"
	Session.Timeout=10
%>
<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->  
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->  
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->  
<head>
    <title>Biznet Technovillage | Contact</title>

	<!--#INCLUDE FILE="../include/meta-tag-en.asp"-->
	
	<!--#INCLUDE FILE="../include/google-analytics.asp" -->
	
	<!--#INCLUDE FILE="../include/global-style.asp"-->
	<link rel="stylesheet" href="/assets/plugins/sky-forms/version-2.0.1/css/custom-sky-forms.css">
	
	<!--[if lt IE 9]>
	<link rel="stylesheet" href="assets/plugins/sky-forms/version-2.0.1/css/sky-forms-ie8.css">
	<![endif]-->
	<!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
	<!--[if lt IE 9]>
	<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
	<![endif]-->
</head> 

<body class="contact-page-custom">
	<div class="wrapper contact-page">
		<!--#INCLUDE FILE="../inc-header.asp"-->
		
		<!--=== Breadcrumbs ===-->
		<div class="breadcrumbs">
			<div class="container">
				<h1 class="pull-left">Contact</h1>
				<ul class="pull-right breadcrumb">
					<li><a href="/">Home</a></li>
					<li class="active">Contact</li>
				</ul>
			</div><!--/container-->
		</div><!--/breadcrumbs-->
		<!--=== End Breadcrumbs ===-->
		
		<!-- Google Map -->
		<div id="map-canvas" class="map"></div>
		<!-- End Google Map -->

		<!--=== Content Part ===-->
		<div class="container content">
			<div class="row margin-bottom-20">
			<div class="col-md-12 mb-margin-bottom-20">
	
    <div class="headline-center-v2 margin-bottom-60">
            <h2><span class="color-green">LOCATION</span></h2>
            <span class="bordered-icon"><i class="fa fa-th-large"></i></span>
            <p>Located in Cimanggis, about 35 KM South of Jakarta Central Business District (CBD) and 20 KM North of Bogor. Close to major universities (University of Indonesia, Gunadarma University, etc), about 10 KM West of Biznet Technovillage and can be reached in 20 minutes. About 60 minutes from Jakarta Soekarno-Hatta International Airport. Excellent toll road, public road and public transportation. Located within <a class="link" target="_blank" href="http://www.riverside-golf.com">Riverside Golf & Country Club</a>. Shuttle bus from Jakarta CBD (MidPlaza) and nearby bus terminal.<p>
            </div>
            <!-- Magazine News -->
            <div class="magazine-news">
                <div class="row">
                    <div class="col-md-6">
	                    <h3 class="color-green">From Jakarta Central Business District (CBD) Area Exit Cimanggis</h3>
	                    <ul class="list-unstyled">
			                <li><i class="fa fa-check color-green"></i> Go to Jakarta Inner City Toll Road towards Bogor/Jagorawi Toll Road direction.</li>
			                <li><i class="fa fa-check color-green"></i> After you reach Jagorawi Toll Road, drive to Exit 19 - Cimanggis (KM 19), follow the ramp until the Toll Plaza.</li>
			                <li><i class="fa fa-check color-green"></i> Drive straight and after 150 M, make a U turn towards the small road on the right. You can see the Bukit Golf or Riverside Golf signage too.</li>
			                <li><i class="fa fa-check color-green"></i> Drive the road after 1 KM, you pass the 1 lane underpass, then drive again around 800 M, then make right once you reach Bukit Golf/Riverside Golf Main Entrance. Please continue the direction Bukit Golf/Riverside Golf Main Entrance - Biznet Technovillage on the next page</li>
			            </ul>
			            </div>
                    <div class="col-md-6">
                        <h3 class="color-green">From Jakarta Central Business District (CBD) Area Exit Cibubur</h3>
                        <ul class="list-unstyled">
	                    	<li><i class="fa fa-check color-green"></i> Go to Jakarta Inner City Toll Road towards Bogor/Jagorawi Toll Road direction.</li>
	                    	<li><i class="fa fa-check color-green"></i> After you reach Jagorawi Toll Road, drive to Exit 12 - Cibubur (KM 12), follow the ramp until the Toll Plaza.</li>
	                    	<li><i class="fa fa-check color-green"></i> Drive straight and go to Alternatif Cibubur street by following Cileungsi/Cikeas signage.</li>
	                    	<li><i class="fa fa-check color-green"></i> Drive the road after 5 KM, you will find Mitra Keluarga hospital on your left side then make a U turn.</li>
	                    	<li><i class="fa fa-check color-green"></i> Drive straight and make left at the first lane.</li>
	                    	<li><i class="fa fa-check color-green"></i> Follow the road then make left once you reach Bukit Golf/Riverside Golf Main Entrance.</li>
	                    	<li><i class="fa fa-check color-green"></i> Drive straight and turn right after 2,5 KM. You will also find Riverside Golf sign and Technovillage sign on your right side.</li>
	                    	<li><i class="fa fa-check color-green"></i> Enter through the Riverside Golf Entrance Gate, then make left towards Biznet Technovillage.</li>
	                    	<li><i class="fa fa-check color-green"></i> Drive straight 1 KM, you will find Biznet Technovillage Entrance Gate.</li>
	                   </ul>
	                   </div>
                </div>
            </div>
            <!-- End Magazine News -->

            <div class="margin-bottom-35"><hr class="hr-md"></div>
            
            <!-- Magazine News -->
            <div class="magazine-news">
                <div class="row">
                    <div class="col-md-6">
	                    <h3 class="color-green">From Depok</h3>
	                    <ul class="list-unstyled">
			                <li><i class="fa fa-check color-green"></i> Go to Cinere - Jagorawi Toll Road towards Bogor/Jagorawi Toll Road direction.</li>
			                <li><i class="fa fa-check color-green"></i> After you reach Jagorawi Toll Road, drive to Exit 19 - Cimanggis (KM 19), follow the ramp until the Toll Plaza.</li>
			                <li><i class="fa fa-check color-green"></i> Drive straight and after 150 M, make a U turn towards the small road on the right. You can see the Bukit Golf or Riverside Golf signage too.</li>
			                <li><i class="fa fa-check color-green"></i> Drive the road after 1 KM, you pass the 1 lane underpass, then drive again around 800 M, then make right once you reach Bukit Golf/Riverside Golf Main Entrance. Please continue the direction Bukit Golf/Riverside Golf Main Entrance - Biznet Technovillage on the next page.</li>
			            </ul>
                    </div>
                    <div class="col-md-6">
	                    <h3 class="color-green">From Jakarta Soekarno Hatta Airport</h3>
						<ul class="list-unstyled">
							<li><i class="fa fa-check color-green"></i>Go to Airport Toll Road towards Jakarta, then continue to Jakarta Inner City Toll Road towards Bogor/Jagorawi Toll Road direction.</li>
							<li><i class="fa fa-check color-green"></i>After you reach Jagorawi Toll Road, drive to Exit 19 - Cimanggis (KM 19), follow the ramp until the Toll Plaza.</li>
							<li><i class="fa fa-check color-green"></i>Drive straight and after 150 M, make a U turn towards the small road on the right. You can see the Bukit Golf or Riverside Golf signage too.</li>
							<li><i class="fa fa-check color-green"></i>Drive the road after 1 KM, you pass the 1 lane underpass, then drive again around 800 M, then make right once you reach Bukit Golf/Riverside Golf Main Entrance. Please continue the direction Bukit Golf/Riverside Golf Main Entrance - Biznet Technovillage below.</li>
				</ul>               
	               </div>
                </div>
            </div>
            <!-- End Magazine News -->

            <div class="margin-bottom-35"><hr class="hr-md"></div>
            
            <!-- Magazine News -->
            <div class="magazine-news">
                <div class="row">
                    <div class="col-md-6">
	                    <h3 class="color-green">From Bogor</h3>
	                    <ul class="list-unstyled">
			                <li><i class="fa fa-check color-green"></i>Go to Jagorawi Toll Road toward Jakarta, exit at Exit Gunung Putri, follow the ramp until the Toll Plaza.</li>
							<li><i class="fa fa-check color-green"></i>Please follow the driving direction shown on Fig 2. Driving Direction from Jagorawi Toll Road Exit 24 - Gunung Putri to Biznet Technovillage.</li>
			            </ul>            
			        </div>
                    <div class="col-md-6">
                        <h3 class="color-green">From Cibubur</h3>
			            <ul class="list-unstyled">
							<li><i class="fa fa-check color-green"></i>Go to Jagorawi Toll Road, drive towards Bogor to Exit 19 - Cimanggis (KM 19), follow the ramp until the Toll Plaza.</li>
							<li><i class="fa fa-check color-green"></i>Drive straight and after 150 M, make a U turn towards the small road on the right. You can see the Bukit Golf or Riverside Golf signage too.</li>
							<li><i class="fa fa-check color-green"></i>Drive the road after 1 KM, you pass the 1 lane underpass, then drive again around 800 M, then make right once you reach Bukit Golf/Riverside Golf Main Entrance. Please continue the direction Bukit Golf/Riverside Golf Main Entrance - Biznet Technovillage below.</li>
							<li><i class="fa fa-check color-green"></i>Drive straight 2.4 KM and keep on right side, you will see Riverside Golf sign and make right.</li>
							<li><i class="fa fa-check color-green"></i>Enter through the Riverside Golf Entrance Gate, then make left towards Biznet Technovillage.</li>
							<li><i class="fa fa-check color-green"></i>Drive straight 1 KM, you will see Biznet Technovillage building on the right side.</li>
						</ul>
                    </div>
                </div>
            </div>
            <!-- End Magazine News -->
            <div class="margin-bottom-35"><hr class="hr-md"></div>
            
            <!-- End Magazine News -->
					<div class="headline"><h2>Inquiry Form</h2></div>
					<p>Please fill out the form below, our team will get back to you immediately.</p><br />
					<form action="inquiries-save.asp?act=inq" method="post" class="sky-form" id="inquiriesform-en">
						<fieldset>
							<div class="row">
								<div class="col-md-6">
									<section>
										<label class="label">Title</label>
										<div class="inline-group placeafter">
											<div class="radio-group">
												<label class="radio"><input type="radio" name="persontitle" value="Mr."><i class="rounded-x"></i>Mr.</label>
												<label class="radio"><input type="radio" name="persontitle" value="Ms."><i class="rounded-x"></i>Ms.</label>
												<label class="radio"><input type="radio" name="persontitle" value="Mrs."><i class="rounded-x"></i>Mrs.</label>
											</div>
										</div>
									</section>
									<section>
										<label class="label">Full Name</label>
										<label class="input">
											<input type="text" name="fullname" id="fullname">
										</label>
									</section>
									<section>
										<label class="label">Date of Birth</label> 
										<label class="input">
											<i class="icon-append fa fa-calendar"></i>
											<input type="text" name="datebirth" id="date">
										</label>
									</section>
									<section>
										<label class="label">Email</label>
										<label class="input">
											<input type="text" name="email" id="email">
										</label>
									</section>
									<section>
										<label class="label">Mobile Phone</label>
										<label class="input">
											<input type="text" name="mobilenumber" id="mobilenumber">
										</label>
									</section>
									<section>
										<label class="label">Address</label>
										<label class="textarea textarea-resizable">
											<textarea rows="3" name="personaddress" id="personaddress"></textarea>
										</label>
									</section>
								</div>
								<div class="col-md-6">
									<section>
										<label class="label">Company Name</label>
										<label class="input">
											<input type="text" name="companyname" id="companyname">
										</label>
									</section>
									<section>
										<label class="label">Company Industry</label>
										<label class="select">
											<select name="companyindustry" id="companyindustry">
												<option value="">-</option>
												<option value="Agriculture">Agriculture</option>
												<option value="Banking">Banking</option>
												<option value="Construction">Construction</option>
												<option value="Consumer Goods">Consumer Goods</option>
												<option value="E-Commerce">E-Commerce</option>
												<option value="Education">Education</option>
												<option value="Financial non-banking">Financial non-banking</option>
												<option value="Gaming">Gaming</option>
												<option value="Government">Government</option>
												<option value="Health">Health</option>
												<option value="Insurance">Insurance</option>
												<option value="Manufacture">Manufacture</option>
												<option value="Media">Media</option>
												<option value="Oil and Mining">Oil and Mining</option>
												<option value="Property">Property</option>
												<option value="Securities">Securities</option>
												<option value="System Integrator">System Integrator</option>
												<option value="Telecommmunication">Telecommmunication</option>
												<option value="Transportation & Logistic">Transportation & Logistic</option>
												<option value="TV & Broadcasting">TV & Broadcasting</option>
												<option value="Others">Others</option>
											</select>
											<i></i>
										</label>
									</section>
									<section>
										<label class="label">I would like to know more about</label>
										<label class="select">
											<select name="interesttype" id="interesttype">
												<option value="">-</option>
												<option value="Sales">Sales/Biznet Services</option>
												<option value="Media Relations">Press/Media Relations</option>
												<option value="Events">Events/Workshop</option>
												<option value="Partnership Opportunities">Partnership Opportunities</option>
												<option value="Customer Support">Customer Support</option>
												<option value="Others">Others</option>
											</select>
											<i></i>
										</label>
									</section>
									<section>
										<label class="label">Please describe your question</label>
										<label class="textarea textarea-resizable">
											<textarea rows="3" name="interestdesc" id="interestdesc"></textarea>
										</label>
									</section>
									<section>
										<label class="label">Embed code shown character</label>
										<label class="input input-captcha">
											<img src="../../../include/captcha.asp" alt="Captcha image" id="imgCaptcha" />
											<input type="text" name="txtCaptcha" id="txtCaptcha">
											<div class="change-captcha-code"><a href="javascript:void(0);">Change Code</a></div>
										</label>
									</section>
									<section>
										<input type="hidden" name="languagepath" id="languagepath" value="<%=language%>">
										<button type="submit" class="btn-u btn-u-" name="btnsubmit">Submit</button>
										<div class="progress"></div>
									</section>								
								</div>
							</div>                  
						</fieldset>
					</form>
				</div>
			</div>
				
		</div>

		<!--#INCLUDE FILE="../inc-footer.asp"-->
	</div><!--/End Wrapepr-->

	<!--#INCLUDE FILE="../include/global-script.asp"-->

	<!-- JS Implementing Plugins -->
	<script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=true"></script>
	<script type="text/javascript" src="/assets/plugins/gmap/gmap.js"></script>
	<script type="text/javascript" src="/assets/plugins/flexslider/jquery.flexslider-min.js"></script>
	<!-- Sky Form -->
	<script src="/assets/plugins/sky-forms/version-2.0.1/js/jquery.form.min.js"></script>
	<!-- Datepicker Form -->
	<script src="/assets/plugins/sky-forms/version-2.0.1/js/jquery-ui.min.js"></script>
	<!-- Validation Form -->
	<script src="/assets/plugins/sky-forms/version-2.0.1/js/jquery.validate.min.js"></script>

	<!-- JS Page Level -->
	<script type="text/javascript" src="/assets/js/app.js"></script>
	<script type="text/javascript" src="/assets/js/forms/inquiries.js"></script>

	<script type="text/javascript">
		var map;
		var iconBase = 'https://www.mymax3.net/images/icons/';

		function initialize() {
			var mapOptions = {
				zoom: 18,
				center: new google.maps.LatLng(-6.435297, 106.896920),
				mapTypeId: google.maps.MapTypeId.SATELLITE
			};
			map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);

			var contentString = '<div style="width:200px;" id="info-window-container">'+
			'<div style="text-align:center;" id="info-window-wrapper">'+
				'<h5>Biznet Technovillage</h5>'+
			'</div>'+
			'</div>';
			var storeLatLng = new google.maps.LatLng(-6.435297, 106.896920);
			var title = 'Biznet Green Data Center';

			createMarker(storeLatLng, title, contentString);
					
		}

		// createMarker function
		function createMarker(storeLatLng, title, contentString){
			var marker = new google.maps.Marker({
				position: storeLatLng,
				map: map, 
				title:title
			});

			var infowindow = new google.maps.InfoWindow({
				content: contentString
			});

			google.maps.event.addListener(marker, 'click', function(){
				infowindow.open(map,marker);
			});
		}

		google.maps.event.addDomListener(window, 'load', initialize);
		
		function RefreshImage(valImageId){
			var objImage = document.getElementById(valImageId);
			if (objImage == undefined){
				return;
			}
			var now = new Date();
			objImage.src = objImage.src.split('?')[0] + '?x=' + now.toUTCString();
		}
		
		jQuery(document).ready(function() {
			App.init();
			Datepicker.initDatepicker();
			InquiriresForm.initInquiriresForm();                              
		});
		
		$(document).ready(function(){
			equalHeight($(".branch"));
			$(".change-captcha-code a").click(function(){
				RefreshImage("imgCaptcha");
			})
		});
	</script>
	<!--[if lt IE 9]>
		<script src="//assets/plugins/respond.js"></script>
		<script src="//assets/plugins/html5shiv.js"></script>
		<script src="//assets/js/plugins/placeholder-IE-fixes.js"></script>
	<![endif]-->

</body>
</html> 
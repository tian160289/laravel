<%
Public function RemoveUnSafeChar(strChar)
    'remove unsafe character to prevent injection
	if strChar<>"" then
		textValid=replace(strChar,"'","''")
		textValid=replace(textValid,"<","")
		textValid=replace(textValid,">","")
		textValid=replace(textValid,"--","")
		textValid=replace(textValid,";","")
	end if
    RemoveUnSafeChar=trim(textValid)
End function

strConnLive = "DRIVER=SQL Server; SERVER=202.169.33.202; UID=radread; PWD=123456; DATABASE=biznet"
Set ConnLive = Server.CreateObject("ADODB.Connection") 
ConnLive.Open strConnLive

'Public Const strConnDev = "DRIVER=SQL Server; SERVER=192.168.0.45; UID=sa; PWD=b1zn3t; DATABASE=biznet"
'Set ConnDev = Server.CreateObject("ADODB.Connection") 
'ConnDev.Open strConnDev

set rs=server.createobject("adodb.recordset")

function schno(TourDate)
	'SCH2012-07AGT-00001
	theday = day(TourDate)
	themonth = MonthName(month(TourDate))
	theyear = year(TourDate)
	if theday > 0 then
		scday = "0" & theday
	elseif theday >= 10 then
		scday = theday
	end if
	scmonth = Left(themonth,3)

	sqlSel="SELECT TOP 1 (substring(right(scheduleno,5),3,3)+1) AS sch FROM [dbo].[Technovillage_Schedule_Tour_Dacen] ORDER by [DateofTour] DESC"
	set rs=ConnDev.execute(sqlSel)
	if not rs.eof then
		schno="SCH" & theyear & "-" & scday & scmonth & "-" & sprintf(rs("sch"))
	else
		schno="SCH" & theyear & "-" & scday & scmonth & "-" & "00001"
	end if
	set rs=nothing
	response.write "scheduleno : " & (scheduleno) & "<br />"
end function

function sprintf(thenum)
	if thenum>0 then
		sprintf="0000" & thenum
	end if
	if thenum>=10 then
		sprintf="000" & thenum
	end if
	if thenum>=100 then
		sprintf="00" & thenum
	end if
	if thenum>=1000 then
		sprintf="0" & thenum
	end if
	if thenum>=10000 then
		sprintf=thenum
	end if
end function

function getlastid(typex)
	'0100001
	'H-10210-00001
	'sql="SELECT top 1 (substring(right(id,5),3,3)+1) as idx FROM [202.169.33.202].Biznet.dbo.max3tvreg  "
	sql="SELECT top 1 (substring(right(id,5),3,3)+1) as idx FROM max3tvreg "
	sql=sql & "WHERE left(id,8)='" & typex & "' ORDER by id DESC"
	'response.Write sql
	set rs=ConnDev.execute(sql)
	if not rs.eof then
		getlastid=typex & sprintf(rs("idx"))
	else
		getlastid=typex & "00001"
	end if
	'response.write "typex : " & (typex) & "<br />"
	'response.write "getlastid : " & (getlastid) & "<br />"
	set rs=nothing
end function

function grabhtml(url)
	' must be an external reference !
	set xmlhttp = server.CreateObject("MSXML2.ServerXMLHTTP")
	xmlhttp.open "GET", url, false
	xmlhttp.send ""
	grabhtml=xmlhttp.ResponseText
	set xmlhttp = nothing
end function

function sendmail(frommail,mailto,mailcc,Subject,msg)
	sch = "http://schemas.microsoft.com/cdo/configuration/"
	Set cdoConfig = CreateObject("CDO.Configuration")
	With cdoConfig.Fields
		.Item(sch & "sendusing") = 2 ' cdoSendUsingPort
		.Item(sch & "smtpserver") = "smtp5.biz.net.id"
		'.Item(sch & "smtpserver") = "smtp.biz.net.id"
		'.Item(sch & "smtpserver") = "smtp.biznetnetworks.com"
		.update
	End With
	'on error resume next
	Set cdoMessage = CreateObject("CDO.Message")
	'response.write destination_Email
	With cdoMessage
		Set .Configuration = cdoConfig
		.From =frommail
		.To = mailto
		if mailcc<>"" then .cc=mailcc
			''.bcc ="irwin_oktoringganis@biznetnetworks.com"
			'.bcc ="max3_care@mymax3.net;irwin_oktoringganis@biznetnetworks.com"
			.Subject = Subject
			.HTMLBody =  msg
			.Send
		End With
	Set cdoMessage = nothing
end function

Public function getValue(sfield, stable, scondition,strConn)
	dim objComm
	set objComm=server.createobject("adodb.connection")
	set rsGetVal=server.createobject("adodb.recordset")

	objComm.open strConn
    SQLCmd = "SELECT " & sfield & " FROM " & stable & " WHERE " & scondition
	response.write SQLCmd

	rsGetVal.open SQLcmd,objComm,3
	if not rsGetVal.Eof then getValue = rsGetVal(0)
	rsGetVal.Close
	set rsGetVal = Nothing
	set objComm=Nothing
end function

%>
<%
	Dim lblResult, lblColor

	if session("inquiries")="" then
	%>
		<script type="text/javascript">
			alert("Your session has expired, Please to repeat your inquiries");
		</script>
		<%
			Response.Redirect "/contact/"
		%>
	<%
	else
		if IsEmpty(Session("ASPCAPTCHA")) or Trim(Session("ASPCAPTCHA")) = "" then
		%>
			<script type="text/javascript">
				window.history.back();
			</script>
		<%
		else
			Dim TestValue : TestValue = Trim(Request.Form("txtCaptcha"))
			'//Uppercase fix for turkish charset//
			TestValue = Replace(TestValue, "i", "I", 1, -1, 1)
			TestValue = Replace(TestValue, "?", "I", 1, -1, 1)
			TestValue = Replace(TestValue, "?", "I", 1, -1, 1)
			'////////////////////
			TestValue = UCase(TestValue)

			if StrComp(TestValue, Trim(Session("ASPCAPTCHA")), 1) = 0 then
				lblResult = "CAPTCHA PASSED"

				act = RemoveUnSafeChar(request("act"))
					'response.write "act : " & act & "<br />"
				fpersontitle = RemoveUnSafeChar(request.form("persontitle"))
					'response.write "persontitle : " & fpersontitle & "<br />"
				ffullname = RemoveUnSafeChar(request.form("fullname"))
					'response.write "fullname : " & ffullname & "<br />"
				fpersonfullname = fpersontitle & " " & ffullname
					'response.write "personfullname : " & fpersonfullname & "<br />"
				fdatebirth = RemoveUnSafeChar(request.form("datebirth"))
					'response.write "datebirth : " & fdatebirth & "<br />"
				fmobilenumber = RemoveUnSafeChar(request.form("mobilenumber"))
					'response.write "mobilenumber : " & fmobilenumber & "<br />"
				femail = RemoveUnSafeChar(request.form("email"))
					'response.write "email : " & femail & "<br />"
				fpersonaddress = RemoveUnSafeChar(request.form("personaddress"))
					'response.write "personaddress : " & fpersonaddress & "<br />"
				fcompanyname = RemoveUnSafeChar(request.form("companyname"))
					'response.write "companyname : " & fcompanyname & "<br />"
				fcompanyindustry = RemoveUnSafeChar(request.form("companyindustry"))
					'response.write "companyindustry : " & fcompanyindustry & "<br />"
				finteresttype = RemoveUnSafeChar(request.form("interesttype"))
					'response.write "interesttype : " & finteresttype & "<br />"
				finterestdesc = RemoveUnSafeChar(request.form("interestdesc"))
					'response.write "interestdesc : " & finterestdesc & "<br />"

				if act="inq" then
					'Create InquiriesID
					sqlSel="SELECT TOP 1 (SUBSTRING(LEFT([InquiriesID],8),4,5)+1) as VID FROM [dbo].[Technovillage_Inquiries] ORDER BY [InquiriesID] DESC"
					set rs = ConnLive.execute(sqlSel)
					if not rs.eof then
						InquiriesID = "Inq" & sprintf(rs("VID"))
					else
						InquiriesID = "Inq00001"
					end if

					sqlIns="INSERT INTO [dbo].[Technovillage_Inquiries]([InquiriesID], [Title], [Fullname], [Datebirth], [MobileNumber], [Email], [Address], [Company], [CompanyIndustry], [InterestType], [InterestDesc], [InquiriesDate]) VALUES('" & InquiriesID & "', '" & fpersontitle & "', '" & ffullname & "', '" & fdatebirth & "', '" & fmobilenumber & "', '" & femail & "', '" & fpersonaddress & "', '" & fcompanyname & "', '" & fcompanyindustry & "', '" & finteresttype & "', '" & finterestdesc & "', getdate())"
					ConnLive.execute(sqlIns)

					call SendEmailConf(InquiriesID,fpersontitle,ffullname,femail,fdatebirth,fmobilenumber,fpersonaddress,fcompanyname,fcompanyindustry,finteresttype)

					' // SEND EMAIL CONFIRMATION
					Sub SendEmailConf(sInquiriesID,sTitle,sFullname,sEmail,sDatebirth,sMobileNumber,sAddress,sCompany,sCompanyIndustry,sInterestType)

						MailSubject=""
						MailBody=""

						sDatebirth = Day(sDatebirth) & " " & MonthName(Month(sDatebirth)) & " " & Year(sDatebirth)

						strSQL="select * from Email_Event_Registration where MailID='CONF-Inquiries-Techno'"
						set rsIn=server.createobject("adodb.recordset")
						rsIn.open strSQL,ConnLive,1,3
						if not rsIn.eof then
							MailSubject=rsIn("MailSubject")
							MailBody=rsIn("MailBody")
						end if
						rsIn.close
						set rsIn=nothing

						MailBody=replace(MailBody,"$Date$",FormatDateTime(date(),1))
						MailBody=replace(MailBody,"$Cust_Title$",sTitle)
						MailBody=replace(MailBody,"$Cust_Name$",sFullname)
						MailBody=replace(MailBody,"$Cust_Company$",sCompany)
						MailBody=replace(MailBody,"$Date_Birth$",sDatebirth)
						MailBody=replace(MailBody,"$Mobile_Number$",sMobileNumber)
						MailBody=replace(MailBody,"$Address$",sAddress)
						MailBody=replace(MailBody,"$Email$",sEmail)
						MailBody=replace(MailBody,"$Company_Name$",sCompany)
						MailBody=replace(MailBody,"$Company_Industry$",sCompanyIndustry)
						MailBody=replace(MailBody,"$Interest_Type$",sInterestType)

						if sInterestType = "Sales" then
							'Mailcc = "Datacenter <datacenter@biznetnetworks.com>;Adindhana Dasaad <adindhana_dasaad@biznetnetworks.com>;Ferryus Seftanto <ferryus_seftanto@biznetnetworks.com>;Dwiyan Nugroho <dwiyan_nugroho@biznetnetworks.com>;Destria Sitompul <destria_sitompul@biznetdatacenter.com>"
						elseif sInterestType = "Media Relations" OR sInterestType = "Others" then
							'Mailcc = "Corporate Communication <corporate_communication@biznetnetworks.com>"
						elseif sInterestType = "Events" OR sInterestType = "Tour Facility Technovillage" then
							'Mailcc = "Marketing Event <marketing_event@biznetnetworks.com>;brand@biznetnetworks.com;Corporate Communication <corporate_communication@biznetnetworks.com>"
						elseif sInterestType = "Partnership Opportunities" then
							'Mailcc = "Corporate Communication <corporate_communication@biznetnetworks.com>;brand@biznetnetworks.com"
						elseif sInterestType = "Customer Support" then
							'Mailcc = "Biznet Premiere Care <premiere_care@biznetnetworks.com>;customer care <customer_care@biznetNetworks.com>"
						end if

						sch = "http://schemas.microsoft.com/cdo/configuration/"
						Set cdoConfig = server.CreateObject("CDO.Configuration")						
						With cdoConfig.Fields 
							.Item(sch & "sendusing") = 2 
							.Item(sch & "smtpserver") = "smtp5.biz.net.id"
							.Item("urn:schemas:mailheader:X-Priority") = 1
							.update
						End With
						
						on error resume next

						Set cdoMessage = server.CreateObject("CDO.Message")
						With cdoMessage
							Set .Configuration = cdoConfig
							.From ="Datacenter <datacenter@biznetnetworks.com>"
							'.From ="reinaldo_manopo@biznetnetworks.com"
							.To = sEmail
							.cc = Mailcc
							.bcc ="reinaldo_manopo@biznetnetworks.com;destria_sitompul@biznetdatacenter.com;catur_pamungkas@biznetnetworks.com"
							.Subject = MailSubject
							.Fields("urn:schemas:httpmail:importance").Value = 2
							.HTMLBody = MailBody
							.Fields.Update()
							.Send
						End With

						Set cdoMessage = Nothing
						Set cdoConfig = Nothing
					end sub

					session("dcinquires")=""
					Session("ASPCAPTCHA")=""
					Response.Redirect "/contact/?act=done"
				else
					Response.Redirect "/contact"
				end if
			else
				%>
				<script type="text/javascript">
					alert("Invalid Captcha code");
					window.history.back();
				</script>
				<%
			end if
		end if
	end if
%>
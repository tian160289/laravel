<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->  
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->  
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->  
<head>
    <title>Biznet Technovillage | Integrated High Tech Facility In Cimanggis, West Java</title>

    <!--#INCLUDE FILE="include/meta-tag-en.asp"-->

	<!--#INCLUDE FILE="include/global-style.asp"-->

	<!--#INCLUDE FILE="include/google-analytics.asp" -->

	<!--#INCLUDE FILE="include/connect.asp"-->

	<!-- CSS Implementing Page -->
	<link rel="stylesheet" href="/assets/plugins/revolution-slider/rs-plugin/css/settings.css" type="text/css" media="screen">
	<!--[if lt IE 9]><link rel="stylesheet" href="/assets/plugins/revolution-slider/rs-plugin/css/settings-ie8.css" type="text/css" media="screen"><![endif]-->
</head>	

<body>
<div class="wrapper page-option-v1 home-page">
	<!--#INCLUDE FILE="inc-header.asp"-->

    <!--=== Slider ===-->
    <div class="tp-banner-container">
        <div class="tp-banner">
            <ul>
				<!-- SLIDE -->
                <li class="revolution-mch-1" data-transition="fade" data-slotamount="5" data-masterspeed="1000" data-title="Biznet Technovillage">
                    <!-- MAIN IMAGE -->
                    <img src="/assets/img/technovillage/main_biznet_technovillage_backview.jpg"  alt="Biznet Technovillage" data-bgfit="cover" data-bgposition="left center" data-bgrepeat="no-repeat">

                    <div class="tp-caption revolution-ch1 sft start"
                        data-x="center"
                        data-hoffset="0"
                        data-y="100"
                        data-speed="1500"
                        data-start="500"
                        data-easing="Back.easeInOut"
                        data-endeasing="Power1.easeIn"                        
                        data-endspeed="300">
						Biznet Technovillage
                    </div>

                    <!-- LAYER -->
                    <div class="tp-caption revolution-ch2 sft"
                        data-x="center"
                        data-hoffset="0"
                        data-y="190"
                        data-speed="1400"
                        data-start="2000"
                        data-easing="Power4.easeOut"
                        data-endspeed="300"
                        data-endeasing="Power1.easeIn"
                        data-captionhidden="off"
                        style="z-index: 6">
						An integrated high tech facility consists of<br />Tier-3 Data Center, Grade A Office Space, and<br />Full Service Business Center
                    </div>

                    <!-- LAYER -->
                    <div class="tp-caption sft"
                        data-x="center"
                        data-hoffset="0"
                        data-y="360"
                        data-speed="1600"
                        data-start="2800"
                        data-easing="Power4.easeOut"
                        data-endspeed="300"
                        data-endeasing="Power1.easeIn"
                        data-captionhidden="off"
                        style="z-index: 6">
                        <a href="/concept/" class="btn-u btn-brd btn-brd-hover btn-u-light">Learn More</a>
                    </div>
                </li>
                <!-- END SLIDE -->

				<!-- SLIDE -->
                <li class="revolution-mch-1" data-transition="fade" data-slotamount="5" data-masterspeed="1000" data-title="Grade A Office Space">
                    <!-- MAIN IMAGE -->
                    <img src="/assets/img/technovillage/OfficeSpace.jpg"  alt="Grade A Office Space" data-bgfit="cover" data-bgposition="left top" data-bgrepeat="no-repeat">

                    <div class="tp-caption revolution-ch1 sft start"
                        data-x="center"
                        data-hoffset="0"
                        data-y="100"
                        data-speed="1500"
                        data-start="500"
                        data-easing="Back.easeInOut"
                        data-endeasing="Power1.easeIn"                        
                        data-endspeed="300">
						Grade A Office Space
                    </div>

                    <!-- LAYER -->
                    <div class="tp-caption revolution-ch2 sft"
                        data-x="center"
                        data-hoffset="0"
                        data-y="190"
                        data-speed="1400"
                        data-start="2000"
                        data-easing="Power4.easeOut"
                        data-endspeed="300"
                        data-endeasing="Power1.easeIn"
                        data-captionhidden="off"
                        style="z-index: 6">
						Premium Office Space for research center,<br />backup facility, media production and<br />outsourcing center
                    </div>

                    <!-- LAYER -->
                    <div class="tp-caption sft"
                        data-x="center"
                        data-hoffset="0"
                        data-y="360"
                        data-speed="1600"
                        data-start="2800"
                        data-easing="Power4.easeOut"
                        data-endspeed="300"
                        data-endeasing="Power1.easeIn"
                        data-captionhidden="off"
                        style="z-index: 6">
                        <a href="/grade-a-office-space/" class="btn-u btn-brd btn-brd-hover btn-u-light">Learn More</a>
                    </div>
                </li>
                <!-- END SLIDE -->

				<!-- SLIDE -->
                <li class="revolution-mch-1" data-transition="fade" data-slotamount="5" data-masterspeed="1000" data-title="Biznet Techspace">
                    <!-- MAIN IMAGE -->
                    <img src="/assets/img/main/techspace_office.jpg" alt="Biznet Techspace" data-bgfit="cover" data-bgposition="left top" data-bgrepeat="no-repeat">

                    <div class="tp-caption revolution-ch1 sft start"
                        data-x="center"
                        data-hoffset="0"
                        data-y="100"
                        data-speed="1500"
                        data-start="500"
                        data-easing="Back.easeInOut"
                        data-endeasing="Power1.easeIn"                        
                        data-endspeed="300">
						Biznet Techspace
                    </div>

                    <!-- LAYER -->
                    <div class="tp-caption revolution-ch2 sft"
                        data-x="center"
                        data-hoffset="0"
                        data-y="190"
                        data-speed="1400"
                        data-start="2000"
                        data-easing="Power4.easeOut"
                        data-endspeed="300"
                        data-endeasing="Power1.easeIn"
                        data-captionhidden="off"
                        style="z-index: 6">
						Full Service Business Center
                    </div>

                    <!-- LAYER -->
                    <div class="tp-caption sft"
                        data-x="center"
                        data-hoffset="0"
                        data-y="310"
                        data-speed="1600"
                        data-start="2800"
                        data-easing="Power4.easeOut"
                        data-endspeed="300"
                        data-endeasing="Power1.easeIn"
                        data-captionhidden="off"
                        style="z-index: 6">
                        <a href="/grade-a-office-space"/" class="btn-u btn-brd btn-brd-hover btn-u-light">Learn More</a>
                    </div>
                </li>
                <!-- END SLIDE -->
            </ul>
            <div class="tp-bannertimer tp-bottom"></div>            
        </div>
    </div>
    <!--=== End Slider ===-->

	<div class="container content-sm">
		<div class="headline-center-v2 margin-bottom-60">
            <h2>About <span class="color-green">Biznet Technovillage</span></h2>
            <span class="bordered-icon"><i class="fa fa-th-large"></i></span>
            <p>Biznet Technovillage is an integrated high tech facility located 35 KM South of Jakarta, consists of Tier-3 Green Data Center, Grade A Office Space and Full Service Business Center targeted for research center, backup facility, media production and outsourcing center.</p>
			<p>Biznet Technovillage is using several green building technologies to reduce waste and power such as furniture material from existing pallet wood, LED based light bulbs to reduce power consumption, Dynamic Rotary Uninterruptible Power Supply to provide 100% backup power without using any lead (Pb) based battery. Lead, at certain exposure levels, is a poisonous substance to animals as well as for human beings. The facility also reviewed by Green Building Council Indonesia for certification.</p>
        </div>

        <div class="row">
            <div class="col-sm-4">
                <div class="service-block-v1 md-margin-bottom-50">
                    <i class="rounded-x icon-cloud-upload"></i>
                    <h3 class="title-v3-bg text-uppercase">Data Center</h3>
                    <p>A Tier-3 and PCI DSS Green Data Center with high quality, reliable and scalable solutions.</p>
                    <a href="/green-data-center" class="text-uppercase">Read More</a>
                </div>    
            </div>
            <div class="col-sm-4">
                <div class="service-block-v1 md-margin-bottom-50">
                    <i class="rounded-x icon-users"></i>
                    <h3 class="title-v3-bg text-uppercase">Office Space</h3>
                    <p>Grade A Office Space with comfortable environment.</p>
                    <a href="/grade-a-office-space" class="text-uppercase">Read More</a>
                </div>    
            </div>
            <div class="col-sm-4">
                <div class="service-block-v1 md-margin-bottom-50">
                    <i class="rounded-x icon-briefcase"></i>
                    <h3 class="title-v3-bg text-uppercase">Business Center</h3>
                    <p>Full Service Business Center with secretarial facility.</p>
                    <a href="/business-center" class="text-uppercase">Read More</a>
                </div>    
            </div>
        </div>
    </div>

	<div class="bg-grey content-sm padding-bottom-0">
		<div class="container">
			<div class="headline-center-v2">
				<h2>Biznet Technovillage Facility Tour</h2>
				<span class="bordered-icon"><i class="fa fa-th-large"></i></span>
				<p>Want to know more about one of Biznet's most advanced facility? Please <a href="http://www.biznetnetworks.com/en/company/technovillage-tour/" target="_blank">register here</a></p>
			</div>
		</div>
		<ul class="list-unstyled row portfolio-box-v2 margin-bottom-0">
			<li class="col-sm-3">
				<div href="/assets/img/facilities/biznet-technovillage-javascript.jpg" title="Javascript" data-rel="fancybox-button" class="fancybox-button">
					<img alt="Biznet Technovillage Javascript" src="/assets/img/facilities/biznet-technovillage-javascript.jpg" class="img-responsive">
					<span class="portfolio-box-v2-in">
						<i class="rounded-x icon-magnifier-add"></i>
					</span>
				</div>    
			</li>
			<li class="col-sm-3">
				<div href="/assets/img/facilities/biznet-technovillage-techspace.jpg" title="Techspace" data-rel="fancybox-button" class="fancybox-button">
					<img alt="Biznet Technovillage Techspace" src="/assets/img/facilities/biznet-technovillage-techspace.jpg" class="img-responsive">
					<span class="portfolio-box-v2-in">
						<i class="rounded-x icon-magnifier-add"></i>
					</span>
				</div>    
			</li>
			<li class="col-sm-3">
				<div href="/assets/img/facilities/biznet-technovillage-voc.jpg" title="Video Operation Center" data-rel="fancybox-button" class="fancybox-button">
					<img alt="Biznet Technovillage VOC" src="/assets/img/facilities/biznet-technovillage-voc.jpg" class="img-responsive">
					<span class="portfolio-box-v2-in">
						<i class="rounded-x icon-magnifier-add"></i>
					</span>
				</div>    
			</li>
			<li class="col-sm-3">
				<div href="/assets/img/facilities/biznet-technovillage-closedrack.jpg" title="Secure Closed Rack Space" data-rel="fancybox-button" class="fancybox-button">
					<img alt="Biznet Technovillage Closed Rack" src="/assets/img/facilities/biznet-technovillage-closedrack.jpg" class="img-responsive">
					<span class="portfolio-box-v2-in">
						<i class="rounded-x icon-magnifier-add"></i>
					</span>
				</div>    
			</li>
		</ul>
	</div>

	<div class="container content-sm">
		<div class="row">
			<!-- Welcome Block -->
			<div class="col-md-8 md-margin-bottom-40">
				<div class="headline"><h2>Welcome To Biznet Technovillage</h2></div>
				<div class="row">
					<div class="col-sm-4">
						<img alt="" src="/assets/img/team/PD_Adi_Kusma.jpg" class="img-responsive margin-bottom-20">
					</div>
					<div class="col-sm-8">
						<p>The high-tech business facility is now available for enterprises and small medium companies, who need high tech, yet enviromentally friendly, business facility to support their business growth and development.</p>
						<p class="margin-bottom-0">Biznet Technovillage offer:</p>
						<ul class="list-unstyled margin-bottom-20">
							<li><i class="fa fa-check color-green"></i> Reliable and scable solutions</li>
							<li><i class="fa fa-check color-green"></i> Strategic location</li>
							<li><i class="fa fa-check color-green"></i> Healthy working environtment</li>
						</ul>                    
					</div>
				</div>

				<blockquote class="hero-unify">
					<p>"Those are some of the efforts that we have done to make sure that Biznet Technovillage can be the best facility in Indonesia. It will set a new level of modern office facility and data center in Indonesia."</p>
					<small>said Adi Kusma, President Director - Biznet</small>
				</blockquote>
			</div><!--/col-md-8-->        

			<!-- Posts -->
			<div class="col-md-4 posts">
				<div class="headline"><h3>Recent Events</h3></div>
				<%
				sqlEvent="SELECT top 3 [Date Display] as DateDisplay, Events, Location, [Event Id] as id FROM [Biznet_Events] WHERE Datediff(month,[Date Start],getdate())<= 3 AND Technovillage='Y' ORDER BY [Date Start] DESC"
				set rsgetEvent=server.createobject("adodb.recordset")
				rsgetEvent.Open sqlEvent,ConnBiznet,1,3
				if not rsgetEvent.EOF then
					while not rsgetEvent.EOF 
					%>
					<dl class="dl-horizontal">
						<dd style="margin-left:0;">
							<p class="bold"><i class="fa fa-calendar-o"></i> <%=rsgetEvent("DateDisplay")%></p>
							<p>
								<%=rsgetEvent("Events")%><br />
								<i class="fa fa-map-marker"></i> <%=rsgetEvent("Location")%>
							</p>
						</dd>
					</dl>
					<%
					rsgetEvent.MoveNext
					wend
				end if
				rsgetEvent.Close
				set rsgetEvent = nothing
				set rsgetEvent = nothing
				%>
			</div>
		</div>
	</div>

    <!--#INCLUDE FILE="inc-footer.asp"-->
</div><!--/wrapper-->

<!--#INCLUDE FILE="include/global-script.asp"-->

<!-- JS Implementing Page -->
<script type="text/javascript" src="/assets/plugins/revolution-slider/rs-plugin/js/jquery.themepunch.tools.min.js"></script>
<script type="text/javascript" src="/assets/plugins/revolution-slider/rs-plugin/js/jquery.themepunch.revolution.min.js"></script>

<!-- JS Page Level -->
<script type="text/javascript" src="/assets/js/plugins/revolution-slider.js"></script>
<script type="text/javascript">
    jQuery(document).ready(function(){
        RevolutionSlider.initRSfullWidth();
    });
</script>
<!--[if lt IE 9]>
	<script src="/assets/plugins/respond.js"></script>
	<script src="/assets/plugins/html5shiv.js"></script>
	<script src="/assets/js/plugins/placeholder-IE-fixes.js"></script>
<![endif]-->

</body>
</html>
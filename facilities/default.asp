<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->  
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->  
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->  
<head>
    <title>Biznet Technovillage | Facilities</title>

	<!--#INCLUDE FILE="../include/meta-tag-en.asp"-->

    <!--#INCLUDE FILE="../include/global-style.asp"-->

	<!--#INCLUDE FILE="../include/google-analytics.asp"-->

	<!--#INCLUDE FILE="../include/connect.asp"-->

	<!-- CSS Implementing Page -->
    <link rel="stylesheet" href="/assets/plugins/revolution-slider/rs-plugin/css/settings.css" type="text/css" media="screen">
    <!--[if lt IE 9]><link rel="stylesheet" href="/assets/plugins/revolution-slider/rs-plugin/css/settings-ie8.css" type="text/css" media="screen"><![endif]-->
</head> 

<body>
<div class="wrapper">
	<!--#INCLUDE FILE="../inc-header.asp"-->

	<!--=== Breadcrumbs ===-->
    <div class="breadcrumbs">
        <div class="container">
            <h1 class="pull-left">Facilities</h1>
            <ul class="pull-right breadcrumb">
                <li><a href="/">Home</a></li>
                <li class="active">Facilities</li>
            </ul>
        </div><!--/container-->
    </div><!--/breadcrumbs-->
    <!--=== End Breadcrumbs ===-->

	<!--=== Slider ===-->
    <div class="tp-banner-container">
        <div class="tp-banner">
            <ul>
				<!-- SLIDE -->
                <li class="revolution-mch-1" data-transition="fade" data-slotamount="5" data-masterspeed="1000" data-title="Slide 1">
                    <!-- MAIN IMAGE -->
                    <img src="/assets/img/technovillage/drups.jpg"  alt="Biznet Technovillage DRUPS" data-bgfit="cover" data-bgposition="right center" data-bgrepeat="no-repeat">

                    <div class="tp-caption revolution-ch1 sft start"
                        data-x="center"
                        data-hoffset="0"
                        data-y="100"
                        data-speed="1500"
                        data-start="500"
                        data-easing="Back.easeInOut"
                        data-endeasing="Power1.easeIn"                        
                        data-endspeed="300">
						Green Power Supply
                    </div>

                    <!-- LAYER -->
                    <div class="tp-caption revolution-ch2 sft"
                        data-x="center"
                        data-hoffset="0"
                        data-y="190"
                        data-speed="1400"
                        data-start="2000"
                        data-easing="Power4.easeOut"
                        data-endspeed="300"
                        data-endeasing="Power1.easeIn"
                        data-captionhidden="off"
                        style="z-index: 6">
						Maximum Energy Efficient.<br />Minimum Environmental Impact
                    </div>

                </li>
                <!-- END SLIDE -->

            </ul>
            <div class="tp-bannertimer tp-bottom"></div>            
        </div>
    </div>
    <!--=== End Slider ===-->

    <!--=== Content Part ===-->
	<div class="container content">
		<div class="headline-center-v2 margin-bottom-60">
            <h2>World Class <span class="color-green">Facility</span></h2>
            <span class="bordered-icon"><i class="fa fa-th-large"></i></span>
            <p>Biznet Technovillage uses the global standard equipments and technologies in Data Center and Office Building facilities.</p>
        </div>

        <div class="row content-boxes-v2 margin-bottom-30">
            <div class="col-md-4 md-margin-bottom-30">                
                <h2 class="heading-sm">
                    <a class="link-bg-icon" href="#">
                        <i class="icon-custom icon-sm rounded-x icon-bg-dark fa fa-certificate"></i>
                        <span>Tier-3 Certified Green Data Center Space</span>
                    </a>
                </h2>
            </div>
            <div class="col-md-4 md-margin-bottom-30">
                <h2 class="heading-sm">
                    <a class="link-bg-icon" href="#">
                        <i class="icon-custom icon-sm rounded-x icon-bg-dark fa fa-star-o"></i> 
                        <span>Grade A Office Space</span>
                    </a>
                </h2>
            </div>
             <div class="col-md-4 md-margin-bottom-30">
                <h2 class="heading-sm">
                    <a class="link-bg-icon" href="#">
                        <i class="icon-custom icon-sm rounded-x icon-bg-dark fa fa-desktop"></i> 
                        <span>Full Service Business Center</span>
                    </a>
                </h2>
            </div>
        </div>

        <div class="row content-boxes-v2 margin-bottom-60">
			<div class="col-md-4">
                <h2 class="heading-sm">
                    <a class="link-bg-icon" href="#">
                        <i class="icon-custom icon-sm rounded-x icon-bg-dark fa fa-glass"></i>
                        <span>Dining Lounge</span>
                    </a>
                </h2>
            </div>
            <div class="col-md-4 md-margin-bottom-30">                
                <h2 class="heading-sm">
                    <a class="link-bg-icon" href="#">
                        <i class="icon-custom icon-sm rounded-x icon-bg-dark fa fa-coffee"></i>
                        <span>Coffee Shop</span>
                    </a>
                </h2>
            </div>
            <div class="col-md-4 md-margin-bottom-30">
                <h2 class="heading-sm">
                    <a class="link-bg-icon" href="#">
                        <i class="icon-custom icon-sm rounded-x icon-bg-dark fa fa-bolt"></i> 
                        <span>Fiber Optic Telecom Infrastructure</span>
                    </a>
                </h2>
            </div>
        </div>

		<div class="row content-boxes-v2 margin-bottom-60">
            <div class="col-md-4 md-margin-bottom-30">
                <h2 class="heading-sm">
                    <a class="link-bg-icon" href="#">
                        <i class="icon-custom icon-sm rounded-x icon-bg-dark fa fa-database"></i> 
                        <span>Full Backup Power System</span>
                    </a>
                </h2>
            </div>
            <div class="col-md-4">
                <h2 class="heading-sm">
                    <a class="link-bg-icon" href="#">
                        <i class="icon-custom icon-sm rounded-x icon-bg-dark fa fa-soccer-ball-o"></i>
                        <span>Sport Facilities</span>
                    </a>
                </h2>
            </div>
			<div class="col-md-4">
                <h2 class="heading-sm">
                    <a class="link-bg-icon" href="#">
                        <i class="icon-custom icon-sm rounded-x icon-bg-dark fa fa-link"></i>
                        <span>Carrier Neutral</span>
                    </a>
                </h2>
            </div>
        </div>
		
		<div class="tag-box tag-box-v2">
            <p>For more information about Biznet Techspace and services, please contact our Account Executive via phone +62-21-57998888, email <a class="link" href="mailto:techspace@biznetnetworks.com">techspace@biznetnetworks.com</a> or visit our nearest <a href="http://www.biznetnetworks.com/en/company/contact">branch office</a> in your city.</p>
            <p>For Biznet Technovillage facility tour, please fill out the <a href="http://www.biznetnetworks.com/en/company/technovillage-tour">registration form</a>.</p>
        </div>
	</div>

	<!--#INCLUDE FILE="../inc-footer.asp"-->
</div><!--/End Wrapepr-->

<!--#INCLUDE FILE="../include/global-script.asp"-->

<!-- JS Implementing page -->
<script type="text/javascript" src="/assets/plugins/jquery.parallax.js"></script>
<script type="text/javascript" src="/assets/plugins/revolution-slider/rs-plugin/js/jquery.themepunch.tools.min.js"></script>
<script type="text/javascript" src="/assets/plugins/revolution-slider/rs-plugin/js/jquery.themepunch.revolution.min.js"></script>

<!-- JS Page Level -->           
<script type="text/javascript" src="/assets/js/plugins/revolution-slider.js"></script>
<script type="text/javascript">
    jQuery(document).ready(function() {
        App.initParallaxBg();        
        RevolutionSlider.initRSfullWidth();
    });
</script>
<!--[if lt IE 9]>
    <script src="/assets/plugins/respond.js"></script>
    <script src="/assets/plugins/html5shiv.js"></script>
    <script src="/assets/js/plugins/placeholder-IE-fixes.js"></script>
<![endif]-->

</body>
</html> 
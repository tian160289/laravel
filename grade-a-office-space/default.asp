<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->  
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->  
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->  
<head>
    <title>Biznet Technovillage | Grade A Office Space</title>

	<!--#INCLUDE FILE="../include/meta-tag-en.asp"-->

    <!--#INCLUDE FILE="../include/global-style.asp"-->

	<!--#INCLUDE FILE="../include/google-analytics.asp"-->

	<!--#INCLUDE FILE="../include/connect.asp"-->
	
	<!-- CSS Implementing Page -->
    <link rel="stylesheet" href="/assets/plugins/revolution-slider/rs-plugin/css/settings.css" type="text/css" media="screen">
    <!--[if lt IE 9]><link rel="stylesheet" href="/assets/plugins/revolution-slider/rs-plugin/css/settings-ie8.css" type="text/css" media="screen"><![endif]-->
</head> 

<body>
<div class="wrapper">
	<!--#INCLUDE FILE="../inc-header.asp"-->

	<!--=== Breadcrumbs ===-->
    <div class="breadcrumbs">
        <div class="container">
            <h1 class="pull-left">Grade A Office Space</h1>
            <ul class="pull-right breadcrumb">
                <li><a href="/">Home</a></li>
                <li class="active">Grade A Office Space</li>
            </ul>
        </div>
    </div><!--/breadcrumbs-->
    <!--=== End Breadcrumbs ===-->

	<!--=== Slider ===-->
    <div class="tp-banner-container">
        <div class="tp-banner">
            <ul>
				<!-- SLIDE -->
                <li class="revolution-mch-1" data-transition="fade" data-slotamount="5" data-masterspeed="1000" data-title="Slide 1">
                    <!-- MAIN IMAGE -->
                    <img src="/assets/img/main/grade-a-office-space/Techspace.jpg" alt="Closed Rack Space" data-bgfit="cover" data-bgposition="center top" data-bgrepeat="no-repeat">

                    <div class="tp-caption revolution-ch1 sft start"
                        data-x="center"
                        data-hoffset="0"
                        data-y="100"
                        data-speed="1500"
                        data-start="500"
                        data-easing="Back.easeInOut"
                        data-endeasing="Power1.easeIn"                        
                        data-endspeed="300">
						Grade A Office Space
                    </div>

                    <!-- LAYER -->
                    <div class="tp-caption revolution-ch2 sft"
                        data-x="center"
                        data-hoffset="0"
                        data-y="190"
                        data-speed="1400"
                        data-start="2000"
                        data-easing="Power4.easeOut"
                        data-endspeed="300"
                        data-endeasing="Power1.easeIn"
                        data-captionhidden="off"
                        style="z-index: 6">
						Store your mission critical system and applications
                    </div>

                </li>
                <!-- END SLIDE -->

            </ul>
            <div class="tp-bannertimer tp-bottom"></div>            
        </div>
    </div>
    <!--=== End Slider ===-->

    <!--=== Content Part ===-->
    <div class="container content"> 
		<div class="headline-center-v2 margin-bottom-60">
            <h2>Grade A <span class="color-green">Office Space</span></h2>
            <span class="bordered-icon"><i class="fa fa-th-large"></i></span>
            <p>Biznet Technovillage offers 5,000 m2 of Grade A Premium Quality Office Space in a spacious green environment that will inspire creative people working in the facility. With direct access to Biznet Fiber and Biznet Data Center Technovillage, it allows you to connect to your Head Office and Branch Offices seamlessly. The facility is suitable for TV and Multimedia Studio, Disaster Recovery Office and R&D Labs, equipped with full backup power generator and cooling systems to support your 24x7 operation.</p>
        </div>

		<div class="blog margin-bottom-40">
        	<!-- Magazine News -->
            <div class="magazine-news">
                <div class="row">
                    <div class="col-md-6">
	                    <h3 class="color-green">Building</h3>
	                    <p>Gross Area: 5,000m2 (55,000 ft2)</p>
	                    <p>Structure:</p>
	                    <ul class="list-unstyled">
	                        <li><i class="fa fa-check color-green"></i> Steel Frame with concrete floors, metal faced cladding and glazing</li>
	                        <li><i class="fa fa-check color-green"></i> Typical floor loading of 300 kg/m2</li>
	                        <li><i class="fa fa-check color-green"></i> 4.0 m slab to slab height</li>
	                        <li><i class="fa fa-check color-green"></i> Column spacing primarily based on 8m grid</li>
						</ul>
	                    <p> Lift:</p>
						<ul class="list-unstyled">
	                        <li><i class="fa fa-check color-green"></i> Passenger Lift 2 x 1 ton</li>
	                    </ul>
                    </div>
                    <div class="col-md-6">
                        <h3 class="color-green">Power</h3>
                        <ul class="list-unstyled">
                        <li><i class="fa fa-check color-green"></i> Redundant 4 x 2.5 MVA power transformer from the Power Grid</li>
                        <li><i class="fa fa-check color-green"></i> For Critical Power Full Backup: N+1 Dynamic Rotary Uninterruptible Power Supply (DRUPS) to provide greener power solution without using any lead (Pb) based battery.</li>
                        <li><i class="fa fa-check color-green"></i> For Non-Critical Power Full Backup: N+1 Generator Sets</li>
                        <li><i class="fa fa-check color-green"></i>	Individual power outlet with 100 percent short circuit protection</li>
                        <li><i class="fa fa-check color-green"></i>	On-site diesel tank support up to 3 days, 24x7 fuel contract delivery callout in place</li>
                        <li><i class="fa fa-check color-green"></i>	Building Management System to monitor and control the power system</li>
                    </ul>
                    </div>
                </div>
            </div>
            <!-- End Magazine News -->

            <div class="margin-bottom-35"><hr class="hr-md"></div>
            
            <!-- Magazine News -->
            <div class="magazine-news">
                <div class="row">
                    <div class="col-md-6">
	                    <h3 class="color-green">Lightning & Grounding System</h3>
	                	<ul class="list-unstyled">
	                    	<li><i class="fa fa-check color-green"></i> Grounding outlets installed throughout the building and data centers</li>
	                    </ul>
                    </div>
                    <div class="col-md-6">
                    	<h3 class="color-green">Environmental System</h3>
                     	<ul class="list-unstyled">
                     		<li><i class="fa fa-check color-green"></i> Individual air conditioning system</li>
                     	</ul>
                    </div>
                </div>
            </div>
            <!-- End Magazine News -->

            <div class="margin-bottom-35"><hr class="hr-md"></div>
            
            <!-- Magazine News -->
            <div class="magazine-news">
                <div class="row">
                    <div class="col-md-6">
	                    <h3 class="color-green">Fire Detection and Suppression System</h3>
	                    <ul class="list-unstyled">
	                        <li><i class="fa fa-check color-green"></i> Fire and smoke detection sensors above the ceiling</li>
	                        <li><i class="fa fa-check color-green"></i> Water based fire fighting system</li>
	                        <li><i class="fa fa-check color-green"></i> Portable Gas based fire fighting system located throughout the facility</li>
	                        <li><i class="fa fa-check color-green"></i> Facility Management Team trained for fire safety and equipment operation with SOP</li>
	                        <li><i class="fa fa-check color-green"></i> Building Management System to monitor and control the fire system</li>
	                    </ul>
                    </div>
                    <div class="col-md-6">
	                    <h3 class="color-green">Telecommunications</h3>
	                    <ul class="list-unstyled">
			                <li><i class="fa fa-check color-green"></i> Separate power and data cable tray</li>
			                <li><i class="fa fa-check color-green"></i> Redundant <a href="http://www.biznetnetworks.com/en/company/network/biznet-fiber">Biznet Fiber</a> between Jakarta CBD to Biznet Technovillage to ensure mission critical operations</li>
			                <li><i class="fa fa-check color-green"></i> Access to multiple telecommunications providers on the Meet Me Room facility, for up to date list please find on the Provider List</li>
			                <li><i class="fa fa-check color-green"></i> Direct connection to <a href="http://www.biznetnetworks.com/en/company/network/biznet-fiber">Biznet Fiber</a> and <a href="http://www.biznetnetworks.com/en/company/network/biznet-global-internet">Biznet Global Internet</a> networks</li>
	                    </ul>
                    </div>
                </div>
            </div>
            <!-- End Magazine News -->
            <div class="margin-bottom-35"><hr class="hr-md"></div>
            
            <!-- Magazine News -->
            <div class="magazine-news">
                <div class="row">
                    <div class="col-md-6">
	                    <h3 class="color-green">Building Management</h3>
                        <ul class="list-unstyled">
	                        <li><i class="fa fa-check color-green"></i> Tailored private office area to customer specification</li>
	                        <li><i class="fa fa-check color-green"></i> Facility Management Team 24x7</li>
                   		</ul>
                    </div>
                    <div class="col-md-6">
                    	<h3 class="color-green">Security</h3>
                       	<ul class="list-unstyled">
	                        <li><i class="fa fa-check color-green"></i> 24x7 professional security guards</li>
	                        <li><i class="fa fa-check color-green"></i> Secure RFID card system with personalized PIN number</li>
	                        <li><i class="fa fa-check color-green"></i> Surveillance color cameras with digital recording facility</li>
                   		</ul>
                    </div>
                </div>
            </div>
            <!-- End Magazine News -->

		</div>
		<!-- End Blog Posts -->

		<div class="tag-box tag-box-v2">
            <p>For more information about Biznet Techspace and services, please contact our Account Executive via phone +62-21-57998888, email <a class="link" href="mailto:techspace@biznetnetworks.com">techspace@biznetnetworks.com</a> or visit our nearest <a href="http://www.biznetnetworks.com/en/company/contact">branch office</a> in your city.</p>
            <p>For Biznet Technovillage facility tour, please fill out the <a href="http://www.biznetnetworks.com/en/company/technovillage-tour">registration form</a>.</p>
        </div>
    </div>
    <!--=== End Content Part ===-->  

 	<!--#INCLUDE FILE="../inc-footer.asp"-->
</div><!--/End Wrapepr-->

<!--#INCLUDE FILE="../include/global-script.asp"-->

<!-- JS Page Level -->           
<script type="text/javascript" src="/assets/plugins/revolution-slider/rs-plugin/js/jquery.themepunch.tools.min.js"></script>
<script type="text/javascript" src="/assets/plugins/revolution-slider/rs-plugin/js/jquery.themepunch.revolution.min.js"></script>
<script type="text/javascript" src="/assets/js/plugins/revolution-slider.js"></script>
<script type="text/javascript">
    jQuery(document).ready(function(){
		RevolutionSlider.initRSfullWidth();
    });
</script>
<!--[if lt IE 9]>
    <script src="/assets/plugins/respond.js"></script>
    <script src="/assets/plugins/html5shiv.js"></script>
    <script src="/assets/js/plugins/placeholder-IE-fixes.js"></script>
<![endif]-->

</body>
</html> 
	<!--=== Footer Version 1 ===-->
    <div class="footer-v1">
        <div class="footer">
            <div class="container">
                <div class="row">
                    <div class="col-md-4 md-margin-bottom-40">
						<div class="headline"><h2>About Biznet</h2></div>
                        <p>Biznet is a company that focuses on telecommunication and multimedia business, with a commitment to build modern infrastructure to reduce Indonesia's digital gap with other developed countries. <a href="http://www.biznetnetworks.com/en/company/about-us/" target="_blank">More Info <i class="fa fa fa-angle-right"></i></a></p>
                    </div>

                    <div class="col-md-4 md-margin-bottom-40">
                        <div class="headline"><h2>Useful Links</h2></div>
                        <ul class="list-unstyled link-list">
                            <li><a href="/concept">Concept</a><i class="fa fa-angle-right"></i></li>
                            <li><a href="/facilities">Facilities</a><i class="fa fa-angle-right"></i></li>
                            <li><a href="/green-data-center">Green Data Center</a><i class="fa fa-angle-right"></i></li>
                            <li><a href="/grade-a-office-space">Grade A Office Space</a><i class="fa fa-angle-right"></i></li>
                            <li><a href="/business-center">Business Center</a><i class="fa fa-angle-right"></i></li>
                            <li><a href="/contact">Contact</a><i class="fa fa-angle-right"></i></li>
                        </ul>
                    </div>

                    <div class="col-md-4 map-img md-margin-bottom-40">
                        <div class="headline"><h2>Contact Us</h2></div>
                        <address class="md-margin-bottom-40">
                            PT. Dinamika Raya Prima<br>
							Jl. Biznet Technovillage No 1<br>
							Cimanggis, West Java 16965 - Indonesia<br>
							Phone : +62-21-29299300<br>
							Fax : +62-21-29299310<br>
                            Email: <a href="mailto:datacenter@biznetnetworks.com" class="">datacenter@biznetnetworks.com</a>
                        </address>

                        <div class="headline"><h2>Contact Center</h2></div>
						<address class="md-margin-bottom-40">
							Hotline: 1500988<br>
							WhatsApp <img align="middle" src="/assets/img/whatsapp_icon_footer.png" position="middle" width="15" height="15"><a href="https://goo.gl/zSfr2j" target="_blank"> +62-855-1998888</a><br/>
						</address>
                    </div>
                </div>
            </div>
        </div><!--/footer-->

        <div class="copyright">
            <div class="container">
                <div class="row">
                    <div class="col-md-9">
                        <p>
							&copy; 2000 - <%=Year(date())%> Biznet Technovillage. All Rights Reserved. <a href="http://www.biznetnetworks.com/en/privacy-policy/" target="_blank">Privacy Policy</a> | <a href="http://www.biznetnetworks.com/en/terms-and-conditions/"  target="_blank">Terms and Conditions</a><br>
							Biznet is part of <a href="http://www.midplaza.com" target="_blank">MidPlaza Holding</a>.
                        </p>
                    </div>

                    <!-- Social Links -->
                    <div class="col-md-3">
                        <ul class="footer-socials list-inline">
                            <li>
                                <a href="http://facebook.com/BiznetHome/" class="tooltips" data-toggle="tooltip" data-placement="top" title="" data-original-title="Facebook">
                                    <i class="fa fa-facebook"></i>
                                </a>
                            </li>
                            <li>
                                <a href="https://www.instagram.com/biznethome/" class="tooltips" data-toggle="tooltip" data-placement="top" title="" data-original-title="Instagram">
                                    <i class="fa fa-instagram"></i>
                                </a>
                            </li>
                        </ul>
                    </div>
                    <!-- End Social Links -->
                </div>
            </div>
        </div><!--/copyright-->
    </div>
    <!--=== End Footer Version 1 ===-->

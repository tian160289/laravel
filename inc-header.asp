	<!--=== Header ===-->    
    <div id="tight-nav" class="header-v3">
        <!-- Navbar -->
        <div class="navbar navbar-default mega-menu" role="navigation">
            <div class="container">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-responsive-collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="fa fa-bars"></span>
                    </button>
                    <a class="navbar-brand" href="/">
						<img id="logo-header" src="/assets/img/logo-biznet-technovillage.png" alt="Biznet Technovillage Logo">
                    </a>
                </div>

                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse navbar-responsive-collapse">
                    <ul class="nav navbar-nav">
                        <!-- Home -->
                        <li class="active">
                            <a href="/">
                                Home
                            </a>
                        </li>
                        <!-- End Home -->

						<!-- Concept -->
                        <li>
                            <a href="/concept/">
                                Concept
                            </a>
                        </li>
                        <!-- End Home -->

						<!-- Facilities -->
                        <li>
                            <a href="/facilities/">
                                Facilities
                            </a>
                        </li>
                        <!-- End Facilities -->

						<!-- Green Data Center -->
                       <li>
                            <a href="/green-data-center/">
                                Green Data Center
                            </a>
                        </li>
                        <!-- End Green Data Center -->

						<!-- Grade A Office Space -->
                        <li>
                            <a href="/grade-a-office-space/">
                                Grade A Office Space
                            </a>
                        </li>
                        <!-- End Grade A Office Space -->

						<!-- Business Center -->
                        <li>
                            <a href="/business-center/">
                                Business Center
                            </a>
                        </li>
                        <!-- End Business Center -->

						<!-- Contact -->
                        <li>
                            <a href="/contact/">
                                Contact
                            </a>
                        </li>
                        <!-- End Contact -->
                       
                    </ul>
                </div><!--/navbar-collapse-->
            </div>    
        </div>            
        <!-- End Navbar -->
    </div>
    <!--=== End Header ===-->
	
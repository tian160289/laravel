	<!-- JS Global Compulsory -->
	<script type="text/javascript" src="/assets/plugins/jquery/jquery.min.js"></script>
	<script type="text/javascript" src="/assets/plugins/jquery/jquery-migrate.min.js"></script>
	<script type="text/javascript" src="/assets/plugins/bootstrap/js/bootstrap.js"></script>
	<!-- JS Implementing Plugins -->
	<script type="text/javascript" src="/assets/plugins/back-to-top.js"></script>
	<script type="text/javascript" src="/assets/plugins/fancybox/source/jquery.fancybox.pack.js"></script>
	<script type="text/javascript" src="/assets/plugins/owl-carousel/owl-carousel/owl.carousel.min.js"></script>
	<!-- JS Customization -->
	<script type="text/javascript" src="/assets/js/custom.js"></script>
	<!-- JS Page Level -->
	<script type="text/javascript" src="/assets/js/apps.js"></script>
	<script type="text/javascript" src="/assets/js/plugins/fancy-box.js"></script>
	<script type="text/javascript" src="/assets/js/plugins/owlcarousel.js"></script>
	<script type="text/javascript">
		jQuery(document).ready(function(){
			App.init();
			FancyBox.initFancybox();
			OwlCarousel.initOwlCarousel();
		});
	</script>
	<!--[if lt IE 9]>
		<script src="/assets/plugins/respond.js"></script>
		<script src="/assets/plugins/html5shiv.js"></script>
		<script src="/assets/js/plugins/placeholder-IE-fixes.js"></script>
	<![endif]-->
	
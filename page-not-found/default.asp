<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->  
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->  
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->  
<head>

	<title>Biznet Technovillage | 404 Error Page</title>
	
	 <!-- Meta -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

	<!--#INCLUDE FILE="../include/google-analytics.asp"-->
    <!--#INCLUDE FILE="../include/global-style.asp"-->

	<!-- CSS Page Style -->    
    <link rel="stylesheet" href="/assets/css/pages/page_404_error.css">
</head> 

<body>
<div class="wrapper error-page">
	<!--#INCLUDE FILE="../inc-header.asp"-->
   <!--=== Breadcrumbs ===-->
    <div class="breadcrumbs">
        <div class="container">
            <h1 class="pull-left">404 Error</h1>
            <ul class="pull-right breadcrumb">
                <li><a href="index.html">Home</a></li>
                <li class="active">404 Error</li>
            </ul>
        </div> 
    </div><!--/breadcrumbs-->
    <!--=== End Breadcrumbs ===-->

    <!--=== Content Part ===-->
    <div class="container content">		
        <!--Error Block-->
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="error-v1">
                    <span class="error-v1-title">Page Not Found</span>
                    <span>We are sorry</span>
                    <p>The requested URL was not found on this server.</p>
                    <a class="btn-u btn-bordered" href="/">Back Home</a>
                </div>
            </div>
        </div>
        <!--End Error Block-->
    </div>	
    <!--=== End Content Part ===-->


    <!--#INCLUDE FILE="../inc-footer.asp"-->
</div><!--/End Wrapepr-->

<!--#INCLUDE FILE="../include/global-script.asp"-->
</body>
</html> 